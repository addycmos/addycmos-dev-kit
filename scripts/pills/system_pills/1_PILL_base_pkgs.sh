#!/bin/bash
## Base PKGs 

## Packages
PKGS=(
	"linux" 
	"linux-firmware" 
	"htop" 
	"vim" 
	"lm_sensors" 
	"tmux" 
	"git" 
	"grub" 
	"connman-dinit" 
	"connman" 
	"wpa_supplicant" 
	"iwd" 
	"openssh" 
	"openssh-dinit" 
	"hwloc"
	"python3"
	"parted"
)

## Install packages
install_pkgs() {
    pacman -S --noconfirm ${PKGS[@]}
    ln -s ../connmand /etc/dinit.d/boot.d
    ln -s ../sshd /etc/dinit.d/boot.d
}

install_pkgs
