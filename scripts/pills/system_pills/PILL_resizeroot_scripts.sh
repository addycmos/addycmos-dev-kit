#!/bin/bash
## Install resizeroot Scripts

## Git repo
REPO="addycmos-files"

## install_files variables
USER="root"
GROUP="root"

# Octal permissions
INIT_OCTAL="644"
SBIN_OCTAL="755"

# Locations
INIT_LOCATION="/etc/dinit.d"
INIT_BOOT_LOCATION="/etc/dinit.d/boot.d"

SBIN_LOCATION="/sbin"

## Location in repo
RS_ROOT_SCRIPTS="${ROOTFS}/tmp/${REPO}/resizeroot_scripts"

## Files
RS_ROOT_FILES=("${RS_ROOT_SCRIPTS}/resizeroot_init" "${RS_ROOT_SCRIPTS}/resizeroot_sbin")

## Names
RSR_INIT="resizeroot_init"
RSR_PRE_SBIN="resizeroot_sbin"
RSR_SBIN="resizeroot"

## Install files
install_files() {
    # Install resizeroot_init script to /etc/dinit.d
    install -o ${USER} -g ${GROUP} -m ${INIT_OCTAL} ${RS_ROOT_FILES[0]} ${INIT_LOCATION}
    # Make a symlink of resizeroot_init to /etc/dinit.d/boot.d
    ln -s ${INIT_LOCATION}/${RSR_INIT} ${INIT_BOOT_LOCATION}

    # Install resizeroot_sbin to /sbin
    install -o ${USER} -g ${GROUP} -m ${SBIN_OCTAL} ${RS_ROOT_FILES[1]} ${SBIN_LOCATION}/${RSR_SBIN}
#    mv ${SBIN_LOCATION}/${RSR_PRE_SBIN} ${SBIN_LOCATION}/${RSR_SBIN}
    # Make executable
    chmod +x ${SBIN_LOCATION}/${RSR_SBIN}
}

install_files
    
    
