#!/bin/bash
## Base Files

## Git repo
REPO="addycmos-files"

## install_files variables
# User and group
USER="root"
GROUP="root"

# Octal permissions
ETC_BASIC_OCTAL="644"
ETC_BASHRCD_OCTAL="644"
ETC_SUDOERS_OCTAL="440"
ETC_GRUB_OCTAL="644"

# Locations
ETC_LOCATION="${ROOTFS}/etc"
ETC_BASHRCD_LOCATION="${ROOTFS}/etc/bash/bashrc.d"
ETC_GRUB_LOCATION="${ROOTFS}/etc/default"

## Array variables
ETC="${ROOTFS}/tmp/${REPO}/etc"
BASHRCD="${ETC}/bash/bashrc.d"

## Files
ETC_BASIC=("${ETC}/hosts" "${ETC}/hostname" "${ETC}/locale.conf" "${ETC}/mkinitcpio.conf" "${ETC}/fstab")
ETC_BASHRCD="${BASHRCD}/artix.bashrc"
ETC_SUDOERS="${ETC}/sudoers"
ETC_GRUB="${ETC}/default/grub"

## Install packages
download_pkg() {
    git clone https://gitlab.com/addycmos/${REPO}.git ${ROOTFS}/tmp/${REPO}
}

## Install files
install_files() {
    # Install basic etc/ files 
    for file in ${ETC_BASIC[@]}; do
	install -o ${USER} -g ${GROUP} -m ${ETC_BASIC_OCTAL} ${file} ${ETC_LOCATION}
    done

    # Install other etc/ files
    install -o ${USER} -g ${GROUP} -m ${ETC_BASHRCD_OCTAL} ${ETC_BASHRCD} ${ETC_BASHRCD_LOCATION}

    install -o ${USER} -g ${GROUP} -m ${ETC_SUDOERS_OCTAL} ${ETC_SUDOERS} ${ETC_LOCATION}

    install -o ${USER} -g ${GROUP} -m ${ETC_GRUB_OCTAL} ${ETC_GRUB} ${ETC_GRUB_LOCATION}
}

download_pkg
install_files
  
