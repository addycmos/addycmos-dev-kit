#!/bin/bash

# Sets up root user and addycmos user
setup_users() {
	# Default password variable
	# Can be changed to whatever you want
	PASSWORD="cmosminer9k"

	# Set root password, create addycmos user, then set addycmos password
	echo root:${PASSWORD} | chpasswd

	useradd -m addycmos
	echo addycmos:${PASSWORD} | chpasswd
}

setup_users
        
