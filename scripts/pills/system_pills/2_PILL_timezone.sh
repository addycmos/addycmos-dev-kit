#!/bin/bash

# Sets up timezone and locale
setup_timezone_locale() {
	# Variables for timezone and local
	TIMEZONE="America/New_York"
	LOCALE="en_US.UTF-8 UTF-8"

	# Set timezone and locale then generate locale
	ln -sf /usr/share/zoneinfo/${TIMEZONE} /etc/localtime
	sed -i 's/#${LOCALE}/${LOCALE}/g' /etc/locale.gen
	locale-gen
}

setup_timezone_locale
