#!/bin/bash
## This script should be taken as a guide for making an addon package for AddyCMOS.
## You can change it however you like and need to.

## Packages
PKGS=("PKG1" "PKG2" "PKG3")

## Files
FILES=("FILE1" "FILE2" "FILE3")

## install_files variables
USER="root"
GROUP="root"
FOOBAR_OCTAL_PERMS="644"
FOOBAR2_OCTAL_PERMS="755"
FOOBAR3_OCTAL_PERMS="754"
FOOBAR_LOCATION="file/location/foobar"
FOOBAR2_LOCATION="file/location/foobar2"
FOOBAR3_LOCATION="file/location/foobar3"

## Install packages
install_pkgs() {
	pacman -S --noconfirm ${PKGS[@]}
}

## Install files
install_files() {
	install -o ${USER} -g ${GROUP} -m ${FOOBAR_OCTAL_PERMS} ${FILES[0]} ${ROOTFS}/${FOOBAR_LOCATION}
 	install -o ${USER} -g ${GROUP} -m ${FOOBAR2_OCTAL_PERMS} ${FILES[1]} ${ROOTFS}/${FOOBAR2_LOCATION}
 	install -o ${USER} -g ${GROUP} -m ${FOOBAR3_OCTAL_PERMS} ${FILES[2]} ${ROOTFS}/${FOORBAR3_LOCATION}
}

install_pkgs
install_files
  
