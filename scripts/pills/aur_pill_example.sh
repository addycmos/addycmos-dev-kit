#!/bin/bash
## This script should be taken as a guide for making an addon package for AddyCMOS.
## You can change it however you like and need to.

## Dependencies
DEPS=("dep1" "dep2" "dep3")

## Package
PKG="PKG"

## Install dependencies
install_deps() {
    pacman -s --noconfirm ${DEPS[@]}
}

## Download package
download_pkg() {
    su addycmos -c "git clone https://aur.archlinux.org/${PKG}.git /tmp/${PKG}"
}

## Create package
create_pkg() {
    cd /tmp/${PKG}
    su addycmos -c "makepkg -C"
}

## Install package
install_pkg() {
    cd /tmp/${PKG}
    pacman -U --noconfirm *.pkg.tar.zst
}

install_deps
download_libs
create_pkg
install_pkg
