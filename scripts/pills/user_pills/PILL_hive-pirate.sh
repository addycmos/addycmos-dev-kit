#!/bin/bash
## Hive Pirate

## User program
USER_PROGRAM="hive-pirate"

## install_files variables
USER="addycmos"
USER_HOME="home/${USER}"

## Install user program
install_user_program() {
    su addycmos -c "git clone https://gitlab.com/WheezyBackports/${USER_PROGRAM}.git ${ROOTFS}/${USER_HOME}/${USER_PROGRAM}"
}

install_user_program
