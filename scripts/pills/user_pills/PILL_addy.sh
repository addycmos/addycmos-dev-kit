#!/bin/bash
## Addy

## User program
USER_PROGRAM="addy"

## install_files variables
USER="addycmos"
USER_HOME="home/${USER}"

## Install user program
install_user_program() {
    su addycmos -c "git clone https://gitlab.com/addycmos/${USER_PROGRAM}.git ${ROOTFS}/${USER_HOME}/${USER_PROGRAM}"
}

install_user_program
