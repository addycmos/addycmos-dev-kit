#!/bin/bash
## Dot Files

## Git repo
REPO="addycmos-files"

## install_files variables
# User and group
USER="addycmos"
GROUP="addycmos"

# Octal permissions
DOTS_OCTAL="644"

# Locations
DOTS_LOCATION="${ROOTFS}/home/${USER}/"

## Array variables
DOTS="${ROOTFS}/tmp/${REPO}/dots"

## Files
DOT_FILES=("${DOTS}/bashrc" "${DOTS}/bash_profile" "${DOTS}/tmux.conf")

## Install files
install_files() {
    # Install addycmos dot files
    for file in ${DOT_FILES[@]}; do
	fn=$(echo ${file} | awk -F "/" '{print "."$NF}')
        install -o ${USER} -g ${GROUP} -m ${DOTS_OCTAL} ${file} ${DOTS_LOCATION}/${fn}
    done
}

install_files
  
