#!/bin/bash
## OpenCL Libs

## Dependencies
DEPS=("gcc" "gcc-libs" "libdrm" "numactl" "ocl-icd" "wget" "fakeroot" "base-devel")

## Packages
PKGS=("ncurses5-compat-libs" "opencl-amd")

## Install dependencies
install_deps() {
    pacman -S --noconfirm ${DEPS[@]}
}

## Download packages
download_pkg() {
    for pkg in ${PKGS[@]}; do
	su addycmos -c "git clone https://aur.archlinux.org/${pkg}.git /tmp/${pkg}"
    done
}

## Install gpg key for ncurses5-compat-libs
install_gpg_key() {
    su addycmos -c "gpg --recv-keys CC2AF4472167BE03"
}

## Install packages
install_pkg() {
    for pkg in ${PKGS[@]}; do
	cd /tmp/${pkg}
	su addycmos -c "makepkg -C"
	pacman -U --noconfirm /tmp/${pkg}/*.pkg.tar.zst
    done
}		 

		 
install_deps
download_pkg
install_gpg_key
install_pkg
