#!/bin/bash
## This script should be taken as a guide for making an addon package for AddyCMOS.
## You can change it however you like and need to.

## Files
FILES=("FILE1" "FILE2" "FILE3")

## install_files variables
USER="addycmos"
USER_HOME="/home/${USER}"
GROUP="addycmos"
FOOBAR_OCTAL_PERMS="644"
FOOBAR2_OCTAL_PERMS="755"
FOOBAR3_OCTAL_PERMS="754"
FOOBAR_LOCATION="file/location/foobar"
FOOBAR2_LOCATION="file/location/foobar2"
FOOBAR3_LOCATION="file/location/foobar3"

## Install files
install_files() {
        install -o ${USER} -g ${GROUP} -m ${FOOBAR_OCTAL_PERMS} ${FILES[0]} ${USER_HOME}/${FOOBAR_LOCATION}
        install -o ${USER} -g ${GROUP} -m ${FOOBAR2_OCTAL_PERMS} ${FILES[1]} ${USER_HOME}/${FOOBAR2_LOCATION}
        install -o ${USER} -g ${GROUP} -m ${FOOBAR3_OCTAL_PERMS} ${FILES[2]} ${USER_HOME}/${FOORBAR3_LOCATION}
}

install_files
