#!/bin/bash

## Scripts dir
SCRIPTS=$(dirname $0)

## Required libs/scripts
. ./${SCRIPTS}/global_vars
. ./${SCRIPTS}/global_functions.sh
. ./${SCRIPTS}/info_messages.sh
. ./${SCRIPTS}/checks.sh

## Pill dirs
SYS="${SCRIPTS}/pills/system_pills"
USER="${SCRIPTS}/pills/user_pills"
AUR="${SCRIPTS}/pills/aur_pills"
EXTRAS="${SCRIPTS}/pills/extras_pills"

## Pillbox Addon Manager ##
pillbox() {
    # Create pill lists
    info_p "Getting index of pills"
    readarray -t SYS_LIST <<<$(ls -1 ${SYS})
    readarray -t USER_LIST <<<$(ls -1 ${USER})
    readarray -t AUR_LIST <<<$(ls -1 ${AUR})
    readarray -t EXTRAS_LIST <<<$(ls -1 ${EXTRAS}) 

    # Copy pills from lists to /tmp in rootfs
    if [ '${SYS_LIST[@]}' != "" ]; then
	info_p "Copying system pills"
	for pill in ${SYS_LIST[@]}; do
	    cp ${SYS}/${pill} ${ROOTFS}/tmp/
	done
    fi

    if [ '${USER_LIST[@]}' != "" ]; then
	info_p "Copying user pills"
	for pill in ${USER_LIST[@]}; do
	    cp ${USER}/${pill} ${ROOTFS}/tmp/
	done
    fi

    if [ '${AUR_LIST[@]}' != "" ]; then
	info_p "Copying AUR pills"
	for pill in ${AUR_LIST[@]}; do
	    cp ${AUR}/${pill} ${ROOTFS}/tmp/
	done
    fi

    if [ '${EXTRAS_LIST[@]}' != "" ]; then
      info_p "Copying extras pills"
      for pill in ${EXTRAS_LIST[@]}; do
          cp ${EXTRAS}/${pill} ${ROOTFS}/tmp/
      done
    fi
    
    # Start running pill scripts
    if [ '${SYS_LIST[@]}' != "" ]; then
	info_p "Running system pills"
	for pill in ${SYS_LIST[@]}; do
	    chroot_rootfs ${ROOTFS} "sh /tmp/${pill}"
	done
    fi

    if [ '${USER_LIST[@]}' != "" ]; then
	info_p "Running user pills"
	for pill in ${USER_LIST[@]}; do
	    chroot_rootfs ${ROOTFS} "sh /tmp/${pill}"
	done
    fi

    if [ '${AUR_LIST[@]}' != "" ]; then
	info_p "Running AUR pills"
	for pill in ${AUR_LIST[@]}; do
	    chroot_rootfs ${ROOTFS} "sh /tmp/${pill}"
	done
    fi

    if [ '${EXTRAS_LIST[@]}' != "" ]; then
      info_p "Running extras pills"
       for pill in ${EXTRAS_LIST[@]}; do
           chroot_rootfs ${ROOTFS} "sh /tmp/${pill}"
       done
    fi
}

pillbox
    


