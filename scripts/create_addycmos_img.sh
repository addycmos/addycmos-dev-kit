#!/bin/bash

## Generate AddyCMOS image 
create_img() {
        # Run checks
	# Run priviledges check
	checks privs

	# Run rootfs check
	checks rootfs exists

	info_m "Starting image build"
    
	# Calculate rootfs size and create raw image
	create_blank_raw_img() {
		info_m "Calculating size raw image should be"
    
	        # Calculate image size and store values
		#
		# ROOTFS_SIZE_IN_BS Get our rootfs size in block size of 4096
		#
		# RESERVE_BYTES Calculate the amount of reserved bytes that need to be added on the image
		# Equation Variables: rsibs=ROOTFS_SIZE_IN_BS bs=4096 p=0.20
		# Equation: a=(rsibs*bs)*p
		# What is happening here is we are multiplying our rootfs size in blocks by our block size first.
		# Then we multiply it by 0.20 to get the 20% reserved bytes that are needed for the ext4 rootfs partition.
		#
		# RB_BS Convert our RESERVE_BYTES to blocks in size of 4096
		#
		# IMG_SIZE Finally calculate our raw image size
		ROOTFS_SIZE_IN_BS=$(du -s -B 4096 ${ROOTFS} | awk '{print $1}')
		echo "Rootfs size in blocks: ${ROOTFS_SIZE_IN_BS}"

		RESERVE_BYTES=$(awk -v rsibs=${ROOTFS_SIZE_IN_BS} -v bs=4096 -v p=0.20 'BEGIN {OFMT="%d"; a=(rsibs*bs)*p; print a}')
		echo "Reserved bytes: ${RESERVE_BYTES}"	
	
		RB_BS=$(echo "$RESERVE_BYTES / 4096" | bc)
		echo "Reserved bytes size in blocks: ${RB_BS}"
	
		IMG_SIZE=$(echo "${ROOTFS_SIZE_IN_BS} + ${RB_BS}" | bc)
		echo "Size of raw image in bytes: ${IMG_SIZE}"

		# Create blank raw image
		info_m "Creating raw image"
		dd if=/dev/zero of=${BUILD_DIR}/addycmos.img bs=4096 count=${IMG_SIZE} status=progress
	}

	# Create bootable raw image from rootfs
	create_bootable_img() {
		# Create loopback
		info_m "Creating loopback device"
		losetup -fP ${BUILD_DIR}/addycmos.img

		# Find loopback
		find_loopback

		# Create partition
		info_m "Creating loopback device partition"
		fdisk ${LOOPBACK} < ${SCRIPTS}/script_files/fdisk_file

		# Create filesystem
		info_m "Creating filesystem"
		mkfs -t ext4 ${LOOPBACK}p1

		# Create mount point
		info_m "Mounting loopback partition"
		mkdir ${MOUNTPOINT}
		mount ${LOOPBACK}p1 ${MOUNTPOINT}

		# Copy rootfs
		info_m "Copying rootfs over to image"
		rsync -ahv ${ROOTFS}/* ${MOUNTPOINT}

		# Mount pseudo filesystems
		mount_pseudo ${MOUNTPOINT}

		# Get loopback UUID
		LOOPBACK_UUID=$(blkid | grep ${LOOPBACK}p1 | cut -d':' -f2 | cut -d'"' -f2)

		# Setup fstab
		info_m "Setting up fstab"
		chroot_rootfs ${MOUNTPOINT} "sed -i 's/ADDYCMOS_UUID/${LOOPBACK_UUID}/g' /etc/fstab"

		# Setup Grub
		info_m "Installing grub bootloader"
		chroot_rootfs ${MOUNTPOINT} "grub-install --target=i386-pc ${LOOPBACK}"
		chroot_rootfs ${MOUNTPOINT} "grub-mkconfig -o /boot/grub/grub.cfg"

		# Get kernel
		KERNEL=$(chroot_rootfs ${MOUNTPOINT} "ls /lib/modules")

		# Setup initramfs
		info_m "Setting up initramfs"
		chroot_rootfs ${MOUNTPOINT} "mkinitcpio root=${LOOPBACK_UUID} -k ${KERNEL} -c /etc/mkinitcpio.conf -g /boot/initramfs-linux.img"
	}

	

	# Create image file
	create_blank_raw_img
	create_bootable_img

	# Run cleanup
	cleanup ${MOUNTPOINT}
}
	
