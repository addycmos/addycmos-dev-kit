#!/bin/bash

## VM test script
vmtest() {
    # Run checks
    # Check priviledges
    checks privs

    # Check if img exists
    checks img exists

    # Check if qemu is installed
    if [ -f '/usr/bin/qemu-system-x86_64' ]; then
	qemu-system-x86_64 -smp cores=2 -m 2048 -drive format=raw,file=${BUILD_DIR}/addycmos.img
    else
	err "Qemu is not installed!"
    fi
}
