#!/bin/bash

## Checks
checks() {
	# Check priviledges
	check_privs() {
		if [ `id -u` != 0 ]; then
			err "Script must be ran with sudo or as root!"
		fi
	}

	# Check if build directory exists if not then create it
	check_build_dir() {
		if [ ! -d ${BUILD_DIR} ]; then
			mkdir ${BUILD_DIR}
		fi
	}
		
	# Check if pseudo filesystems are mounted then unmount them
	check_pseudo_fs_mount() {
		SRC_FS=$1
		
		for PSEUDO in ${PSEUDO_FS[@]}
		do
		        if mountpoint -q "${SRC_FS}/${PSEUDO}"; then
				umount -R -l -f ${SRC_FS}/${PSEUDO} #&>/dev/null
			fi
		done
	}

	# Check if temporary rootfs directory exists
	# If checked for removal then if it exists remove it
	# If checked for exists then doesn't exist then throw error
	check_rootfs() {
		case $1 in
			for_removal)	if [ -d ${ROOTFS} ]; then rm -r ${ROOTFS}; fi;;
			exists)		if [ ! -d ${ROOTFS} ]; then err "Rootfs doesn't exist!"; fi;;
		esac
	}

	# Check for img
	# If checked for removal then if it exists remove it
	# If checked for exists then doesn't exist then throw error
	check_img() {
		case $1 in
			for_removal)	if [ -f ${BUILD_DIR}/addycmos.img ]; then rm ${BUILD_DIR}/addycmos.img; fi;;
			exists)		if [ ! -f ${BUILD_DIR}/addycmos.img ]; then err "Img doesn't exist!"; fi;;
		esac
	}

	# Check if loopback is mounted on mountpoint
	check_loopback_mountpoint() {
		find_loopback
		findmnt ${LOOPBACK} | grep ${MOUNTPOINT} | cut -d' ' -f1
	}    
	
	# Check for loopback and see if it's mounted
	check_loopback() {
		find_loopback
		if [ -b "${LOOPBACK}" ]; then
			if checks loopback_mountpoint == "${MOUNTPOINT}"; then
				umount ${MOUNTPOINT}
			fi

			losetup -d ${LOOPBACK}
		fi
	}

	# Check if mountpoint directory exists
	# If checked for removal then if exists remove it
	# if checked for exists then doesn't exist create it
	check_mountpoint_dir() {
		case $1 in
			for_removal)	if [ -d ${MOUNTPOINT} ]; then rmdir ${MOUNTPOINT}; fi;;
			exists)		if [ ! -d ${MOUNTPOINT} ]; then mkdir ${MOUNTPOINT}; fi;;
		esac
	}
	
	case $1 in
		privs)			check_privs;;
		build_dir)		check_build_dir;;
		rootfs)			check_rootfs $2;;
		pseudo_fs_mount)	check_pseudo_fs_mount $2;;
		img)			check_img;;
		loopback_mountpoint)	check_loopback_mountpoint;;
		loopback)		check_loopback;;
		mountpoint_dir)		check_mountpoint_dir $2;;
	esac
}

