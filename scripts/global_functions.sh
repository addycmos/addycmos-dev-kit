#!/bin/bash

## Mount function for pseudo filesystems
mount_pseudo() {
	DEST=$1
	mount -v --bind /dev ${DEST}/dev
	mount -v --bind /dev/pts ${DEST}/dev/pts
	mount -vt proc proc ${DEST}/proc
	mount -vt sysfs sysfs ${DEST}/sys
	mount -vt tmpfs tmpfs ${DEST}/run
}

## Does chroot command and makes code easier to read
chroot_rootfs() {
	SRC_FS=$1
	COMMAND=${@:2}
	chroot ${SRC_FS} /bin/bash -c "${COMMAND}"
}

## Finds loopback device and stores it into variable
find_loopback() {
	LOOPBACK=$(losetup -j ${BUILD_DIR}/addycmos.img | cut -d":" -f1)
}
	

## Basic cleanup
cleanup() {
	SRC_FS=$1
    
	info_m "Cleaning up ${SRC_FS}"

	# Run pseudo fs check to unmount pseudo filesytems in SRC_FS
	sleep 5
	checks pseudo_fs_mount ${SRC_FS}

	# Run check for loopback device if SRC_FS is MOUNTPOINT
	# Since the loopback check already unmounts the MOUNTPOINT if it's detected
	# we don't need to write another check
	# This also removes the loopback device after creating the image since we run
	# a cleanup after that anyway, so we don't have to make a seperate cleanup
	# function or do any cood gymnastics
	if [ "$SRC_FS" == "${MOUNTPOINT}" ]; then
		checks loopback
	fi
}

