#!/bin/bash

## Generate AddyCMOS rootfs
generate_rootfs() {
	# Run checks
	# Run priviledges check
	checks privs

	# Run check for build directory
	checks build_dir

	# Run pseudo fs check
	checks pseudo_fs_mount ${ROOTFS}

	# Run rootfs check
	checks rootfs for_removal
	 
	# Run mountpoint directory check
	checks mountpoint_dir for_removal

	# Run check for loopback device
	checks loopback

	# Run img check
	checks img for_removal
    
	# Create rootfs directory
	info_m "Creating rootfs directory"
	mkdir -p ${ROOTFS}

	# Run artix bootstrap
	info_m "Running bootstrap"
	sh ${SCRIPTS}/artix-bootstrap/artix-bootstrap.sh -i dinit ${ROOTFS}

	# Mount pseudo filesystems
	info_m "Mounting pseudo filesystems"
	mount_pseudo ${ROOTFS}

	# Run Pillbox
	info_m "Setting up base system with Pillbox"
	sh ${SCRIPTS}/pillbox.sh
	
	# Run cleanup
	cleanup ${ROOTFS}
}

