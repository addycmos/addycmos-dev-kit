#!/bin/bash

## ERR and INFO functions
. ./${SCRIPTS}/script_files/colors # Need pretty colors
err() {
	MESSAGE="$*"
	echo "[${RED}X${NOCOLOR}] ${RED}ERR${NOCOLOR}: ${MESSAGE}"
	exit 1
}

info_m() {
	MESSAGE="$*"
	echo "[${GREEN}INFO${NOCOLOR}] > ${MESSAGE}"
}

info_p() {
	MESSAGE="$*"
	echo "${BLUE}[${GREEN}PILLBOX${BLUE}]${NOCOLOR} > ${MESSAGE}"
}

info_t() {
	MESSAGE="$*"
	echo "${BLUE}[${RED}Tweaker DBG${BLUE}]${NOCOLOR} > ${MESSAGE}"
}


