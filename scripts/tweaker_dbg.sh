#!/bin/bash

## Scripts dir
SCRIPTS=$(dirname $0)

## Required libs/scripts
. ./${SCRIPTS}/global_vars
. ./${SCRIPTS}/global_functions.sh
. ./${SCRIPTS}/info_messages.sh
. ./${SCRIPTS}/prompts.sh
. ./${SCRIPTS}/checks.sh
. ./${SCRIPTS}/generate_rootfs.sh
. ./${SCRIPTS}/create_addycmos_img.sh
. ./${SCRIPTS}/vmtest.sh

## Tool for manual debugging within chroot
dbg_chroot() {
    # Privs check
    checks privs
    
    # Rootfs check
    info_t "Checking if RootFS exists"
    checks rootfs exists

    # Pseudo fs already mounted check
    checks pseudo_fs_mount ${ROOTFS}

    # Mount pseudo filesystems
    info_t "Mounting pseudo filesystems"
    mount_pseudo ${ROOTFS}

    # Chroot into RootFS
    info_t "Entering chroot"
    chroot ${ROOTFS} /bin/bash

    # Unmount pseudo filesystems
    info_t "Unmounting pseudo filesystems"
    checks pseudo_fs_mount ${ROOTFS}
}

## Tool for running and testing pills
dbg_pillbox() {
    # Privs check
    checks privs
    
    # Rootfs check
    info_t "Checking if RootFS exists"
    checks rootfs exists

    # Just run all the pills using pillbox like normal
    run_all_pills() {
	sh ${SCRIPTS}/pillbox.sh
    }

    # Run a single pill using tweaker
    run_single_pill() {
	# Function for running pills in chroot with debug mode
	# Mainly just gives a fancy prompt
	dbg_pill() {
	    chroot ${ROOTFS} /bin/bash -c "PS4=\"${prompt_dbg}\"; set -x; source /tmp/${pill}"
	}
	
	# Pill dirs
	SYS="${SCRIPTS}/pills/system_pills"
	USER="${SCRIPTS}/pills/user_pills"
	AUR="${SCRIPTS}/pills/aur_pills"
	EXTRAS="${SCRIPTS}/pills/extras_pills"

	# Create pill lists
	readarray -t SYS_LIST <<<$(ls -1 ${SYS})
	readarray -t USER_LIST <<<$(ls -1 ${USER})
	readarray -t AUR_LIST <<<$(ls -1 ${AUR})
	readarray -t EXTRAS_LIST <<<$(ls -1 ${EXTRAS})

	# Pill types
	TYPES=("System" "User" "AUR" "Extras");

	# Set prompt
	PS3="${prompt_select}"
	
	# Select type of pill to be ran
	info_t "Select pill type to be ran"
	select pill_type in ${TYPES[@]}; do
	    case ${pill_type} in
		# Select system pill to be ran
		System)
		    info_t "System selected"
		    info_t "Select pill to be ran"
		    select pill in ${SYS_LIST[@]}; do
			case ${pill} in
			    ${pill})
				info_t "${pill} selected"
				
				# Copy pill over to /tmp in RootFS
				info_t "Copying ${pill} over to ${ROOTFS}/tmp"
				cp ${SYS}/${pill} ${ROOTFS}/tmp/

				# Run pill script in RootFS chroot
				info_t "Running ${pill} inside chroot at ${ROOTFS}"
				dbg_pill
				
				break
			    ;;
			esac
		    done
		    break
	        ;;

		# Select user pill to be ran
		User)
		    info_t "User selected"
		    info_t "Select pill to be ran"
		    select pill in ${USER_LIST[@]}; do
			case ${pill} in
			    ${pill})
				info_t "${pill} selected"
				
				# Copy pill over to /tmp in RootFS
				info_t "Copying ${pill} over to ${ROOTFS}/tmp"
				cp ${USER}/${pill} ${ROOTFS}/tmp/

				# Run pill script in RootFS chroot
				info_t "Running ${pill} inside chroot at ${ROOTFS}"
				dbg_pill
				
				break
			    ;;
			esac
		    done
		    break
		;;

		# Select AUR pill to be ran
		AUR)
		    info_t "AUR selected"
		    info_t "Select pill to be ran"
		    select pill in ${AUR_LIST[@]}; do
			case ${pill} in
			    ${pill})
				info_t "${pill} selected"
				
				# Copy pill over to /tmp in RootFS
				info_t "Copying ${pill} over to ${ROOTFS}/tmp"
				cp ${AUR}/${pill} ${ROOTFS}/tmp/

				# Run pill script in RootFS chroot
				info_t "Running ${pill} inside chroot at ${ROOTFS}"
				dbg_pill

				break
			    ;;
			esac
		    done
		    break
		;;
		
		# Select Extras pill to be ran
		Extras)
		    info_t "Extras selected"
		    info_t "Select pill to be ran"
		    select pill in ${EXTRAS_LIST[@]}; do
			case ${pill} in
			    ${pill})
				info_t "${pill} selected"

				# Copy pill over to /tmp in RootFS
				info_t "Copying ${pill} over to ${ROOTFS}/tmp"
				cp ${EXTRAS}/${pill} ${ROOTFS}/tmp/

				# Run pill script in RootFS chroot
				info_t "Running ${pill} inside chroot at ${ROOTFS}"
				dbg_pill

				break
			    ;;
			esac
		    done
		    break
		;;
	    esac
	done
    }
	
    case $1 in
	--run-all-pills)	run_all_pills;;
	--run-pill)		run_single_pill;;
	*)			echo "Please provide --run-all-pills or --run-pill";;
    esac
}

## Tool to run RootFS generation with debugging
dbg_rootfs() {
    # Privs check
    checks privs

    # Run RootFS generation with debugging
    info_t "Generating RootFS with debugging"
    PS4="${prompt_dbg}"
    set -x
    generate_rootfs
    set +x
}

## Tool to run RAW image creation with debugging
dbg_create_img() {
    # Privs check
    checks privs
    
    # Run RAW image creation with debugginga
    info_t "Creating RAW image with debugging"
    PS4="${prompt_dbg}"
    set -x
    create_img
    set +x
}

## Arguments
ARGS=(
    "  --dbg-chroot|tool for manual debugging within chroot  "
    "  --dbg-pillbox (--run-all-pills, --run-pill)|tool for running and testing pills  "
    "  --dbg-rootfs|tool to run RootFS generation with debugging  " 
    "  --dbg-create-img|tool to run RAW image creation with debugging  "
    "  --help, -h|display help  "
)

## Help output
help_output() {
	printf '%s\n\n' "Usage: tweaker_dbg.sh [ARGUMENT] [SUB-ARGUMENT]"
	printf '%s\n' "${ARGS[@]}" | column -t -s'|'
	printf '\n%s\n' "All sub-arguments are mandatory."
	printf '\n%s\n' "Report any bugs to WheezyBackports#7260 on discord." 
}

case $1 in
    --dbg-chroot)	dbg_chroot;;
    --dbg-pillbox)	dbg_pillbox $2;;
    --dbg-rootfs)	dbg_rootfs;;
    --dbg-create-img)	dbg_create_img;;
    --help | -h)	help_output;;
    *)			help_output;;
esac

