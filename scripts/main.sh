#!/bin/bash

## Scripts dir
SCRIPTS=$(dirname $0)

## Required libs/scripts
. ./${SCRIPTS}/global_vars
. ./${SCRIPTS}/global_functions.sh
. ./${SCRIPTS}/info_messages.sh
. ./${SCRIPTS}/checks.sh
. ./${SCRIPTS}/generate_rootfs.sh
. ./${SCRIPTS}/create_addycmos_img.sh
. ./${SCRIPTS}/vmtest.sh

## Arguments
ARGS=(
    "  --generate-rootfs|generate RootFS  "
    "  --create-img|create RAW image  "
    "  --help, -h|display help  "
)

## Help output
help_output() {
        printf '%s\n\n' "Usage: main.sh [ARGUMENT]"
        printf '%s\n' "${ARGS[@]}" | column -t -s'|'
        printf '\n%s\n' "Report any bugs to WheezyBackports#7260 on discord."
}
case $1 in
    --generate-rootfs)	generate_rootfs;;
    --create-img)	create_img;;
    --vmtest)		vmtest;;
    --help | -h)	help_output;;
    *)			help_output;;
esac
