
# Table of Contents

1.  [**AddyCMOS Development Kit**](#addycmos-development-kit)
    1.  [Requirements](#requirements)
    2.  [Getting Started](#getting-started)
    3.  [Future documentation](#future-documentation)


# **AddyCMOS Development Kit**


## Requirements

-   qemu/qemu-system-x86\_64
-   bash 4 or newer
-   make


## Getting Started

First clone the repository like so.

    git clone --recursive https://gitlab.com/addycmos/addycmos-dev-kit.git

To make your own image all you need to do is run sudo make.

    sudo make

You can also generate a rootfs by itself.

    sudo make rootfs

If you already have a rootfs generated you can create an image.

    sudo make img

Once you have an image you can run the vmtest to start a virtual machine in qemu.

    sudo make vmtest


## Future documentation

-   Pillbox addon system
-   Writing pills for Pillbox
-   User programs installed on the distribution
-   Debugging tools when they are finished
-   Installation of AddyCMOS v2

