
# Table of Contents

1.  [AddyCMOS Dev Kit Documentation: Pillbox System](#addycmos-dev-kit-documentation:-pillbox)
    1.  [What is Pillbox?](#what-is-pillbox?)
    2.  [Types of Pills](#types-of-pills)
    3.  [Default Pills](#default-pills)
    4.  [Writing Pills](#writing-pills)
        1.  [Naming Scheme](#naming-scheme)
        2.  [Writing Scheme](#writing-scheme)
        3.  [Hello World Pill](#hello-world-pill)


# AddyCMOS Dev Kit Documentation: Pillbox System


## What is Pillbox?

Pillbox is the addons system created for AddyCMOS V2. Pillbox runs "Pills" during RootFS generation. Pills are merely just a collection
of bash scripts that are run inside the chroot during generation of the RootFS for AddyCMOS. Pillbox simply just creates a list of the
pills located in scripts/pills, copies them over to the RootFS directory located in build/, chroots into the RootFS then runs them.
Pills have no standard to them. You can write them however you like as it's just the same as writing a normal bash script. I will be
providing a guide for writing them in this documentation though as it's easier to write pills if you have a rough template.


## Types of Pills

There are currently 3 types of pills. They are: System Pills, User Pills, and AUR Pills.

System Pills: System Pills are the very first pills ran by Pillbox. They are the most important pills as they are used for system
configuration and creation.

User Pills: User Pills are the second set of pills ran by Pillbox. They are the second most important pills as they are used for
configuration of the addycmos user.

AUR Pills: AUR Pills are the the last set of pills ran by Pillbox. They are the third most important pills as they are used for
installing AUR packages.


## Default Pills

By default pills used to build AddyCMOS are included with the dev kit.

Here is a list of the following pills:
System Pills:

-   1\_PILL\_base\_pkgs.sh:
    Used to install base packages.
-   2\_PILL\_timezone.sh:
    Used to setup default timezone.
-   3\_PILL\_setup\_users.sh:
    Used to setup root and addycmos users.
-   4\_PILL\_base\_files.sh:
    Installs base system files.

User Pills:

-   1\_PILL\_dot\_files.sh:
    Installs addycmos dot files.
-   PILL\_addy.sh:
    Installs Addy miner management program and mining software.
-   PILL\_hive-pirate.sh:
    Installs Hive Pirate GPU overclocking tool.

AUR Pills:

-   PILL\_opencl\_libs.sh:
    Installs OpenCL libraries for AMD GPU mining.


## Writing Pills

Now that you know what pills are already in use and the different types of pills you are ready to start on your own pill.


### Naming Scheme

We must first go over the naming scheme of a pill. The naming scheme of pills are simple they go as follows.

PRIORITY\_PILL\_PILLNAME.sh

PRIORITY: The priority of the pill. This is what determines what order to run pills in. It is similar to how Sys V init runs services.
If the priority is set to 1 then the pill will be the first to execute in the pill set. If it is 2 then it will be the second and so
on. If there is no priority specified then the pill will be the last executed in the set.

PILLNAME: The name of the pill. We can make the name of our pill whatever we would like.


### Writing Scheme

Although there is no standard writing scheme I generally recommend using functions to seperate parts of the pill. I recommend going
through some of the already existing pills to get an idea of how pills should generally be written. Most of the pills contain functions
of similar names, what the pill does, or the name of the pill. Depends on how simple the pill is.

The simplest pill is the 1\_PILL\_base\_pkgs.sh. I will show you the contents of this pill.

    #!/bin/bash
    ## Base PKGs
    
    ## Packages
    PKGS=("linux" "linux-firmware" "htop" "vim" "lm_sensors" "tmux" "git" "grub" "connman-dinit" "connman" "wpa_supplicant" "iwd")
    
    ## Install packages
    install_pkgs() {
            pacman -S --noconfirm ${PKGS[@]}
    }
    
    install_pkgs

As you can see this pill just installs packages used by AddyCMOS. We first start out with an array called "PKGS" and then we have a
function called "install\_pkgs" which gets called at the end of the pill. We will be using this as a guide for our own pill later on.


### Hello World Pill

We can now start on writing our new pill to be ran by Pillbox during RootFS generation. This pill will be installing lolcat and figlet
then outputting some cool ascii.

    #!/bin/bash
    ## Hello World
    
    ## Packages
    PKGS=("lolcat" "figlet")
    
    ## Install packages
    install_pkgs() {
          pacman -S --noconfirm ${PKGS[@]}
    }
    
    ## Run figlet and pipe to lolcat
    cool_ascii() {
          figlet Hello World! | lolcat
    }
    
    install_pkgs
    cool_ascii

This is our finished Hello World pill. Below I'll give a commented version explaining what is going on.

    ## Specify that bash must be used to run this script
    #!/bin/bash
    
    ## Name of our pill
    ## Hello World
    
    ## PKGS array for storing the names of our packages we want to install (Makes things look cleaner later.)
    ## Packages
    PKGS=("lolcat" "figlet")
    
    ## install_pkgs function used for running pacman in scripted mode
    ## Install packages
    install_pkgs() {
          ## What goes on here is that in place of a package name we access our PKGS array from earlier. Instead of having to write
          ## multiple package names we just access all the values stored in the array. This makes things look cleaner if you have 20
          ## or more packages and if you ever wanted to add or subtract packages you don't have to hunt for this function in a large pill.
          pacman -S --noconfirm ${PKGS[@]}
    }
    
    ## cool_ascii function for running figlet and lolcat
    ## We name our functions based on the action they do
    ## Run figlet and pipe to lolcat
    cool_ascii() {
          figlet Hello World! | lolcat
    }
    
    install_pkgs
    cool_ascii

Now that we have written our pill where should we put it? Well, considering we're installing packages to the system we should put it in
scripts/pills/system\_pills/. We should also make sure we are following the naming scheme. Double check and make sure you named your
pill along the lines of "PILL\_hello\_world.sh" or "5\_PILL\_hello\_world.sh" then copy it to scripts/pills/system\_pills/.

Once we have copied our pill over to scripts/pills/system\_pills/ we can now start generating our RootFS and see if it works. Make sure
you're in the same directory as the Makefile for the dev kit.

    sudo make rootfs

While you're waiting on artix-bootstrap to do it's thing keep an eye out for the "[PILLBOX]" info message. This will tell you what
Pillbox is doing.

When Pillbox gets to our Hello World script we should see an output similar to this.

     _   _      _ _        __        __         _     _ _
    | | | | ___| | | ___   \ \      / /__  _ __| | __| | |
    | |_| |/ _ \ | |/ _ \   \ \ /\ / / _ \| '__| |/ _` | |
    |  _  |  __/ | | (_) |   \ V  V / (_) | |  | | (_| |_|
    |_| |_|\___|_|_|\___/     \_/\_/ \___/|_|  |_|\__,_(_)

Congratulations! You have now successfully written your own pill for AddyCMOS!

