* AddyCMOS Dev Kit Documentation: Tweaker DBG

** Getting started
To get started I suggest taking a look at the help options like so.

#+BEGIN_EXAMPLE
  sh scripts/tweaker_dbg.sh --help
#+END_EXAMPLE

You should get an output like this.

#+BEGIN_EXAMPLE
  Usage: tweaker_dbg.sh [ARGUMENT] [SUB-ARGUMENT]
  
    --dbg-chroot                                 tool for manual debugging within chroot
    --dbg-pillbox (--run-all-pills, --run-pill)  tool for running and testing pills
    --dbg-rootfs                                 tool to run RootFS generation with debugging
    --dbg-create-img                             tool to run RAW image creation with debugging
    --help, -h                                   display help
  
  All sub-arguments are mandatory.
  
  Report any bugs to WheezyBackports#7260 on discord.
#+END_EXAMPLE

Now that you become familiar with the help output lets move onto the next section.

** Chroot Manual Debugging
To do manual debugging run tweaker_dbg.sh with --dbg-chroot. This will mount the pseudo filesystems for you and throw you into chroot
for the RootFS. You must run the script as sudo for this!

#+BEGIN_EXAMPLE
  sudo sh scripts/tweaker_dbg.sh --dbg-chroot
#+END_EXAMPLE

There's not much more to talk about here other than you can exit the chroot using "exit". You can do virtually anything inside the
chroot from running commands, writing configs, changing configs, installing software, or debugging shell scripts.

** Running Pills In Debug Mode
Running pills in debug mode is pretty simple. There are two sub-arguments to do so for --dbg-pillbox. All that's required is an
already existing RootFS. Running all available pills will be shown off first and running a single pill second. You will also notice
that when the pills are ran they also show the lines being executed in their scripts. This is used to get an idea of which line could
be causing an issue in a pill.

*** Running All Available Pills
To run all the pills in scripts/pills/ run tweaker_dbg.sh like so. Make sure to use sudo!

#+BEGIN_EXAMPLE
  sudo sh scripts/tweaker_dbg.sh --dbg-pillbox --run-all-pills
#+END_EXAMPLE

You should now see Tweaker DBG invoking Pillbox to run pills inside the RootFS using chroot. You should also start seeing the
respective Pillbox info messages.

*** Running A Single Pill
To run a single pill in scripts/pills run tweaker_dbg.sh like so.

#+BEGIN_EXAMPLE
  sudo sh scripts/tweaker_dbg.sh --dbg-pillbox --run-pill
#+END_EXAMPLE

A selection prompt will now show up looking like this.

#+BEGIN_EXAMPLE
  [Tweaker DBG] > Select pill type to be ran
  1) System
  2) User
  3) AUR
  #?
#+END_EXAMPLE

From here you enter the corresponding number for the pill type to be ran. We will select the "System" type for this example. Once you
do that you will see another prompt.

#+BEGIN_EXAMPLE
  [Tweaker DBG] > Select pill to be ran
  1) 1_PILL_base_pkgs.sh
  2) 2_PILL_timezone.sh
  3) 3_PILL_setup_users.sh
  4) 4_PILL_base_files.sh
  5) PILL_hello_world.sh
  #?
#+END_EXAMPLE

On this prompt you select which pill you want to run in the System type. You do the same as before by entering the corresponding
number. We will select the "hello world" pill from the "Writing Pills" section in the Pillbox documentation. If you have not written
this pill yet I suggest pausing to do so now.

We should now see the selected pill run and give an output like so.

#+BEGIN_EXAMPLE
  [Tweaker DBG] > PILL_hello_world.sh selected
  [Tweaker DBG] > Copying PILL_hello_world.sh over to build/addycmos_rootfs/tmp
  [Tweaker DBG] > Running PILL_hello_world.sh inside chroot at build/addycmos_rootfs
  + PKGS=("lolcat" "figlet")
  + install_pkgs
  + pacman -S --noconfirm lolcat figlet
  warning: lolcat-100.0.1-1 is up to date -- reinstalling
  warning: figlet-2.2.5-4 is up to date -- reinstalling
  resolving dependencies...
  looking for conflicting packages...
  
  Packages (2) figlet-2.2.5-4  lolcat-100.0.1-1
  
  Total Installed Size:  0.76 MiB
  Net Upgrade Size:      0.00 MiB
  
  :: Proceed with installation? [Y/n]
  (2/2) checking keys in keyring                                                    [###############################################] 100%
  (2/2) checking package integrity                                                  [###############################################] 100%
  (2/2) loading package files                                                       [###############################################] 100%
  (2/2) checking for file conflicts                                                 [###############################################] 100%
  :: Processing package changes...
  (1/2) reinstalling lolcat                                                         [###############################################] 100%
  (2/2) reinstalling figlet                                                         [###############################################] 100%
  + cool_ascii
  + figlet Hello 'World!'
  + lolcat
   _   _      _ _        __        __         _     _ _
  | | | | ___| | | ___   \ \      / /__  _ __| | __| | |
  | |_| |/ _ \ | |/ _ \   \ \ /\ / / _ \| '__| |/ _` | |
  |  _  |  __/ | | (_) |   \ V  V / (_) | |  | | (_| |_|
  |_| |_|\___|_|_|\___/     \_/\_/ \___/|_|  |_|\__,_(_)
  
#+END_EXAMPLE

Now that you know how to debug pills we can move onto the next section.

** RootFS Generation In Debug Mode
Generating the RootFS in debug mode is pretty simple. There's not much different compared to what make does. The only difference is
that the output shows the commands which are being run line by line in the script.

To generate a RootFS in debug mode run tweaker_dbg.sh like so.

#+BEGIN_EXAMPLE
  sudo sh scripts/tweaker_dbg.sh --dbg-rootfs
#+END_EXAMPLE

We should now see the RootFS start to generate much like how it does normally.

** RAW Image Generation In Debug Mode
Much like generating our RootFS in debug mode we instead create our RAW image in debug mode. It also shows the debug output like
with --dbg-rootfs.

To create our RAW image in debug mode run tweaker_dbg.sh like so.

#+BEGIN_EXAMPLE
  sudo sh scripts/tweaker_dbg.sh --dbg-create-img
#+END_EXAMPLE

We should now see the RAW image build like how it does normally.


