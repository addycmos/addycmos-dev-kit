
# Table of Contents

1.  [AddyCMOS Dev Kit Documentation: Tweaker DBG](#org7c2fa53)
    1.  [Getting started](#org7f9f2fb)
    2.  [Chroot Manual Debugging](#orgca62317)
    3.  [Running Pills In Debug Mode](#orgc5c176e)
        1.  [Running All Available Pills](#orgd3d046e)
        2.  [Running A Single Pill](#orgd54e30a)
    4.  [RootFS Generation In Debug Mode](#org506ba9f)
    5.  [RAW Image Generation In Debug Mode](#org8d83a76)


<a id="org7c2fa53"></a>

# AddyCMOS Dev Kit Documentation: Tweaker DBG


<a id="org7f9f2fb"></a>

## Getting started

To get started I suggest taking a look at the help options like so.

    sh scripts/tweaker_dbg.sh --help

You should get an output like this.

    Usage: tweaker_dbg.sh [ARGUMENT] [SUB-ARGUMENT]
    
      --dbg-chroot                                 tool for manual debugging within chroot
      --dbg-pillbox (--run-all-pills, --run-pill)  tool for running and testing pills
      --dbg-rootfs                                 tool to run RootFS generation with debugging
      --dbg-create-img                             tool to run RAW image creation with debugging
      --help, -h                                   display help
    
    All sub-arguments are mandatory.
    
    Report any bugs to WheezyBackports#7260 on discord.

Now that you become familiar with the help output lets move onto the next section.


<a id="orgca62317"></a>

## Chroot Manual Debugging

To do manual debugging run tweaker\_dbg.sh with &#x2013;dbg-chroot. This will mount the pseudo filesystems for you and throw you into chroot
for the RootFS. You must run the script as sudo for this!

    sudo sh scripts/tweaker_dbg.sh --dbg-chroot

There's not much more to talk about here other than you can exit the chroot using "exit". You can do virtually anything inside the
chroot from running commands, writing configs, changing configs, installing software, or debugging shell scripts.


<a id="orgc5c176e"></a>

## Running Pills In Debug Mode

Running pills in debug mode is pretty simple. There are two sub-arguments to do so for &#x2013;dbg-pillbox. All that's required is an
already existing RootFS. Running all available pills will be shown off first and running a single pill second. You will also notice
that when the pills are ran they also show the lines being executed in their scripts. This is used to get an idea of which line could
be causing an issue in a pill.


<a id="orgd3d046e"></a>

### Running All Available Pills

To run all the pills in scripts/pills/ run tweaker\_dbg.sh like so. Make sure to use sudo!

    sudo sh scripts/tweaker_dbg.sh --dbg-pillbox --run-all-pills

You should now see Tweaker DBG invoking Pillbox to run pills inside the RootFS using chroot. You should also start seeing the
respective Pillbox info messages.


<a id="orgd54e30a"></a>

### Running A Single Pill

To run a single pill in scripts/pills run tweaker\_dbg.sh like so.

    sudo sh scripts/tweaker_dbg.sh --dbg-pillbox --run-pill

A selection prompt will now show up looking like this.

    [Tweaker DBG] > Select pill type to be ran
    1) System
    2) User
    3) AUR
    #?

From here you enter the corresponding number for the pill type to be ran. We will select the "System" type for this example. Once you
do that you will see another prompt.

    [Tweaker DBG] > Select pill to be ran
    1) 1_PILL_base_pkgs.sh
    2) 2_PILL_timezone.sh
    3) 3_PILL_setup_users.sh
    4) 4_PILL_base_files.sh
    5) PILL_hello_world.sh
    #?

On this prompt you select which pill you want to run in the System type. You do the same as before by entering the corresponding
number. We will select the "hello world" pill from the "Writing Pills" section in the Pillbox documentation. If you have not written
this pill yet I suggest pausing to do so now.

We should now see the selected pill run and give an output like so.

    [Tweaker DBG] > PILL_hello_world.sh selected
    [Tweaker DBG] > Copying PILL_hello_world.sh over to build/addycmos_rootfs/tmp
    [Tweaker DBG] > Running PILL_hello_world.sh inside chroot at build/addycmos_rootfs
    + PKGS=("lolcat" "figlet")
    + install_pkgs
    + pacman -S --noconfirm lolcat figlet
    warning: lolcat-100.0.1-1 is up to date -- reinstalling
    warning: figlet-2.2.5-4 is up to date -- reinstalling
    resolving dependencies...
    looking for conflicting packages...
    
    Packages (2) figlet-2.2.5-4  lolcat-100.0.1-1
    
    Total Installed Size:  0.76 MiB
    Net Upgrade Size:      0.00 MiB
    
    :: Proceed with installation? [Y/n]
    (2/2) checking keys in keyring                                                    [###############################################] 100%
    (2/2) checking package integrity                                                  [###############################################] 100%
    (2/2) loading package files                                                       [###############################################] 100%
    (2/2) checking for file conflicts                                                 [###############################################] 100%
    :: Processing package changes...
    (1/2) reinstalling lolcat                                                         [###############################################] 100%
    (2/2) reinstalling figlet                                                         [###############################################] 100%
    + cool_ascii
    + figlet Hello 'World!'
    + lolcat
     _   _      _ _        __        __         _     _ _
    | | | | ___| | | ___   \ \      / /__  _ __| | __| | |
    | |_| |/ _ \ | |/ _ \   \ \ /\ / / _ \| '__| |/ _` | |
    |  _  |  __/ | | (_) |   \ V  V / (_) | |  | | (_| |_|
    |_| |_|\___|_|_|\___/     \_/\_/ \___/|_|  |_|\__,_(_)

Now that you know how to debug pills we can move onto the next section.


<a id="org506ba9f"></a>

## RootFS Generation In Debug Mode

Generating the RootFS in debug mode is pretty simple. There's not much different compared to what make does. The only difference is
that the output shows the commands which are being run line by line in the script.

To generate a RootFS in debug mode run tweaker\_dbg.sh like so.

    sudo sh scripts/tweaker_dbg.sh --dbg-rootfs

We should now see the RootFS start to generate much like how it does normally.


<a id="org8d83a76"></a>

## RAW Image Generation In Debug Mode

Much like generating our RootFS in debug mode we instead create our RAW image in debug mode. It also shows the debug output like
with &#x2013;dbg-rootfs.

To create our RAW image in debug mode run tweaker\_dbg.sh like so.

    sudo sh scripts/tweaker_dbg.sh --dbg-create-img

We should now see the RAW image build like how it does normally.

