
# Table of Contents

1.  [AddyCMOS Dev Kit Documentation: Dev Kit](#orgdc384bb)
    1.  [Building RootFS](#org48d03b6)
    2.  [Chroot and Manual Modification](#org9938a38)
    3.  [Building A Bootable RAW Image](#orga446285)
    4.  [Launching A Qemu Virtual Machine](#org866d5ea)
    5.  [Advanced](#org0b13738)
        1.  [Scripts/ Tree](#orgee3c882)
        2.  [Setting Global Variables](#orgfaa0e7a)
        3.  [Invoking main.sh Without Make](#orgb89e626)


<a id="orgdc384bb"></a>

# AddyCMOS Dev Kit Documentation: Dev Kit


<a id="org48d03b6"></a>

## Building RootFS

To build a RootFS it is as simple as running make rootfs like so.

    sudo make rootfs


<a id="org9938a38"></a>

## Chroot and Manual Modification

Building the RootFS by itself can be useful for debugging the system or making changes manually. There are now debugging tools, but I
suggest doing this for right now before using Tweaker DBG. Here will be a guide which you can use for making changes manually to the
system.

To setup a proper chroot you are first going to have to mount the pseudo filesystems to the rootfs/ directory found in build/. To do so
what you need to do is run these mount commands on your Linux system.

    mount -v --bind /dev build/rootfs/dev
    mount -v --bind /dev/pts build/rootfs/dev/pts
    mount -vt proc proc /build/rootfs/proc
    mount -vt sysfs sysfs /build/rootfs/sys
    mount -vt tmpfs tmpfs /build/rootfs/run

Once you have mounted the pseudo filesystems you can now chroot into the rootfs.

    cd build/
    sudo chroot rootfs/

Congratulations! You now should be inside the chroot environment! You can run pacman to install specific packages or uninstall specific
packages. You can also create service scripts or do whatever you desire. You can exit by typing "exit" then unmount the pseudo filesystems
like so.

    unmount -R -l -f rootfs/run
    unmount -R -l -f rootfs/sys
    unmount -R -l -f rootfs/proc
    unmount -R -l -f rootfs/dev/pts
    unmount -R -l -f rootfs/dev


<a id="orga446285"></a>

## Building A Bootable RAW Image

To build a bootable RAW image from your already created RootFS run make img like so.

    sudo make img

Your new RAW image should show up in build/ as addycmos.img.


<a id="org866d5ea"></a>

## Launching A Qemu Virtual Machine

To launch our newly created RAW image into a virtual machine run make vmtest like so.

    sudo make vmtest

You should now see a Qemu window launch with the system booting.


<a id="org0b13738"></a>

## Advanced


<a id="orgee3c882"></a>

### Scripts/ Tree

Now that you have mastered the more simple parts of using the dev kit you can now move on to the more advanced part of the kit. I recommend
taking a look at scripts/. This is where you'll find all the scripts used to build the RootFS and RAW image. I will provide a tree for
the directory structure of scripts/.

    scripts/
    ├── artix-bootstrap
    │   ├── README.md
    │   ├── artix-bootstrap.sh
    │   ├── artix-rootfs.sh
    │   └── get-pacman-dependencies.sh
    ├── checks.sh
    ├── create_addycmos_img.sh
    ├── debugger.sh
    ├── generate_rootfs.sh
    ├── global_functions.sh
    ├── global_vars
    ├── info_messages.sh
    ├── main.sh
    ├── pillbox.sh
    ├── pills
    │   ├── aur_pill_example.sh
    │   ├── aur_pills
    │   │   └── PILL_opencl_libs.sh
    │   ├── system_pill_example.sh
    │   ├── system_pills
    │   │   ├── 1_PILL_base_pkgs.sh
    │   │   ├── 2_PILL_timezone.sh
    │   │   ├── 3_PILL_setup_users.sh
    │   │   └── 4_PILL_base_files.sh
    │   ├── user_pill_example.sh
    │   └── user_pills
    │       ├── 1_PILL_dot_files.sh
    │       ├── PILL_addy.sh
    │       └── PILL_hive-pirate.sh
    ├── script_files
    │   ├── colors
    │   └── fdisk_file
    └── vmtest.sh

-   artix-bootstrap/: Contains the scripts used to build a base Artix RootFS for AddyCMOS.

-   checks.sh: Contains the checks used for RootFS generation and RAW image creation.

-   create\_addycmos\_img.sh: Creates our bootable RAW image.

-   generate\_rootfs.sh: Generates our RootFS.

-   global\_functions.sh: Contains the most commonly used functions through out all the scripts.

-   global\_vars: Contains the most commonly used variables through out all the scripts.

-   info\_messages.sh: Info message system used to display pretty messages during builds and used to throw errors.

-   main.sh: Main script that acts as a wrapper used by the other scripts. It is also called by the makefile.

-   pillbox.sh: Pillbox addon system. (See Pillbox documentation for more information.)

-   pills/: Contains all the addons/pills used during RootFS generation (See Pillbox documentation for more information.)

-   script\_files/: Contains certain files needed for the main scripts. "colors" is for pretty colors and "fdisk\_file" contains the fdisk
    commands for create\_addycmos\_img.sh.

-   vmtest.sh: Used for launching the RAW image into a Qemu virtual machine.


<a id="orgfaa0e7a"></a>

### Setting Global Variables

In this section of Advanced we will go over the variables in global\_vars. I will be providing the contents of the file.

    #!/bin/bash
    
    ## Global Variables
    # PSEUDO_FS array for storing names of pseudo filesystems
    PSEUDO_FS=("proc" "sys" "run" "dev/pts" "dev")
    
    # Build directory variable
    # Can be changed if desired
    BUILD_DIR="build"
    
    # ROOTFS variable
    # Can be changed if desired
    ROOTFS="${BUILD_DIR}/addycmos_rootfs"
    
    # MOUNTPOINT variable
    # Can be changed if desired
    MOUNTPOINT="${BUILD_DIR}/rootmnt"

-   PSEUDO\_FS: PSEUDO\_FS is the array for the required pseudo filesystems needed for chroot. It's really only used for check\_pseudo\_fs\_mount in checks.sh.

-   BUILD\_DIR: BUILD\_DIR is the variable used to specify the name and location of our build directory. By default it is just "build".

-   ROOTFS: ROOTFS is the variable used to specify the name and location of our RootFS directory. By default it is "${BUILD\_DIR}/addycmos\_rootfs".

-   MOUNTPOINT: MOUNTPOINT is the variable used to specify the name and location of our mountpoint directory during RAW image creation.
    By default it is just "${BUILD\_DIR}/rootmnt".

As is the defaults are fine. If you have a different setup in mind though you can change these if you would like.


<a id="orgb89e626"></a>

### Invoking main.sh Without Make

In this section of Advanced we will go over using main.sh without using make. If there is a reason you need to do this you now can.
Working with main.sh is very simple as Make is just calls it to begin with. To get the help output of main.sh you can run it with -h.

    sh main.sh -h

You will get the following output back.

    Usage: main.sh [ARGUMENT]
    
    --generate-rootfs  generate RootFS
    --create-img       create RAW image
    --help, -h         display help
    
    Report any bugs to WheezyBackports#7260 on discord.

