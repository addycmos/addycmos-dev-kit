
# Table of Contents

1.  [AddyCMOS Dev Kit Documentation: Modding Pillbox](#orgef66835)
    1.  [Modding Pillbox](#orgefc83f0)
    2.  [Creating Our Pill Type](#org1457640)
        1.  [Creating Pill Type Directory and Copying pillbox.sh](#org0e458d8)
        2.  [Editing Our New Copy](#org294592c)
        3.  [Creating Our Patch](#org94d9900)
        4.  [Applying Our Patch](#org9592c62)
        5.  [Testing Our Patch](#orge7706d6)
    3.  [Modifying Tweaker DBG For Our Pill Type](#org999e8ef)
        1.  [Making A Copy of tweaker\_dbg.sh](#org417604b)
        2.  [Editing Our New Copy](#orgc8ad0f0)
        3.  [Creating Our Patch](#org234d4d3)
        4.  [Applying Our Patch](#org9683630)
        5.  [Testing Our Patch](#orga3ac13f)


<a id="orgef66835"></a>

# AddyCMOS Dev Kit Documentation: Modding Pillbox


<a id="orgefc83f0"></a>

## Modding Pillbox

In this documentation I will be demonstrating modding of Pillbox. Modding is useful when you want to add a new feature or make some
changes that are tailored to your work flow. I will also be documenting the process of adding a new feature so you the user can get an
idea of how to mod Pillbox. We will be using a simple set of tools for this task.


<a id="org1457640"></a>

## Creating Our Pill Type

The feature I want to add is a new pill type called "Extras". This pill type is for any miscellaneous pills for Pillbox to run.


<a id="org0e458d8"></a>

### Creating Pill Type Directory and Copying pillbox.sh

To start creating our new pill type lets create our directory for the new pill type in scripts/pills/

    mkdir scripts/pills/extras_pills

Now that we've created our directory we can now make a copy of scripts/pillbox.sh, so we can make a modified version of it and a patch
we can distribute later.

    cp scripts/pills/pillbox.sh scripts/pills/new_pillbox.sh


<a id="org294592c"></a>

### Editing Our New Copy

Now that we've done that we can start editing our new copy of Pillbox. You can use whatever text editor you like such as vi, vim, or
emacs. I will be using emacs for this. I will be displaying the file, so you can read it in the docs. I will also display the version
with our changes.

1.  Unmodified Pillbox

    This is our unmodified version of Pillbox.
    
        #!/bin/bash
        
        ## Scripts dir
        SCRIPTS=$(dirname $0)
        
        ## Required libs/scripts
        . ./${SCRIPTS}/global_vars
        . ./${SCRIPTS}/global_functions.sh
        . ./${SCRIPTS}/info_messages.sh
        . ./${SCRIPTS}/checks.sh
        
        ## Pill dirs
        SYS="${SCRIPTS}/pills/system_pills"
        USER="${SCRIPTS}/pills/user_pills"
        AUR="${SCRIPTS}/pills/aur_pills"
        
        ## Pillbox Addon Manager ##
        pillbox() {
            # Create pill lists
            info_p "Getting index of pills"
            readarray -t SYS_LIST <<<$(ls -1 ${SYS})
            readarray -t USER_LIST <<<$(ls -1 ${USER})
            readarray -t AUR_LIST <<<$(ls -1 ${AUR}) 
        
            # Copy pills from lists to /tmp in rootfs
            if [ '${SYS_LIST[@]}' != "" ]; then
              info_p "Copying system pills"
              for pill in ${SYS_LIST[@]}; do
          	  cp ${SYS}/${pill} ${ROOTFS}/tmp/
              done
            fi
        
            if [ '${USER_LIST[@]}' != "" ]; then
              info_p "Copying user pills"
              for pill in ${USER_LIST[@]}; do
          	  cp ${USER}/${pill} ${ROOTFS}/tmp/
              done
            fi
        
            if [ '${AUR_LIST[@]}' != "" ]; then
              info_p "Copying AUR pills"
              for pill in ${AUR_LIST[@]}; do
          	  cp ${AUR}/${pill} ${ROOTFS}/tmp/
              done
            fi
        
            # Start running pill scripts
            if [ '${SYS_LIST[@]}' != "" ]; then
              info_p "Running system pills"
              for pill in ${SYS_LIST[@]}; do
          	  chroot_rootfs ${ROOTFS} "sh /tmp/${pill}"
              done
            fi
        
            if [ '${USER_LIST[@]}' != "" ]; then
              info_p "Running user pills"
              for pill in ${USER_LIST[@]}; do
          	  chroot_rootfs ${ROOTFS} "sh /tmp/${pill}"
              done
            fi
        
            if [ '${AUR_LIST[@]}' != "" ]; then
              info_p "Running AUR pills"
              for pill in ${AUR_LIST[@]}; do
          	  chroot_rootfs ${ROOTFS} "sh /tmp/${pill}"
              done
            fi
        }
        
        pillbox

2.  Modified Pillbox

    Now on to our modded version. To add our new pill type we're going to need to add 4 things.
    
    We're going to need to add:
    
    -   A EXTRAS variable that points to our extras\_pills/ directory in scripts/pills
    -   A readarray statement which will read the names of pills into an array called EXTRAS\_LIST
    -   A copy statement that copies the pills over to /tmp in our RootFS
    -   A run statement that runs our pills in the new pill type
    
    1.  Extras Variable
    
        Our EXTRAS variable will look like this alongside the other variables.
        
            ## Pill dirs
            SYS="${SCRIPTS}/pills/system_pills"
            USER="${SCRIPTS}/pills/user_pills"
            AUR="${SCRIPTS}/pills/aur_pills"
            EXTRAS="${SCRIPTS}/pills/extras_pills"
    
    2.  Readarray Statement
    
        Our readarray statement will look like this alongside the other readarray statements.
        
            # Create pill lists
            info_p "Getting index of pills"
            readarray -t SYS_LIST <<<$(ls -1 ${SYS})
            readarray -t USER_LIST <<<$(ls -1 ${USER})
            readarray -t AUR_LIST <<<$(ls -1 ${AUR})
            readarray -t EXTRAS_LIST <<<$(ls -1 ${EXTRAS})
    
    3.  Copy Statement
    
        Our copy statement will look like this alongside the other copy statements.
        
            # Copy pills from lists to /tmp in rootfs
            if [ '${SYS_LIST[@]}' != "" ]; then
              info_p "Copying system pills"
              for pill in ${SYS_LIST[@]}; do
                  cp ${SYS}/${pill} ${ROOTFS}/tmp/
              done
            fi
            
            if [ '${USER_LIST[@]}' != "" ]; then
              info_p "Copying user pills"
              for pill in ${USER_LIST[@]}; do
                  cp ${USER}/${pill} ${ROOTFS}/tmp/
              done
            fi
            
            if [ '${AUR_LIST[@]}' != "" ]; then
              info_p "Copying AUR pills"
              for pill in ${AUR_LIST[@]}; do
                  cp ${AUR}/${pill} ${ROOTFS}/tmp/
              done
            fi
            
            if [ '${EXTRAS_LIST[@]}' != "" ]; then
              info_p "Copying extras pills"
              for pill in ${EXTRAS_LIST[@]}; do
                  cp ${EXTRAS}/${pill} ${ROOTFS}/tmp/
              done
            fi
    
    4.  Run Statement
    
        And lastly our run statement will look like this alongside the other run statements
        
            # Start running pill scripts
            if [ '${SYS_LIST[@]}' != "" ]; then
              info_p "Running system pills"
              for pill in ${SYS_LIST[@]}; do
                  chroot_rootfs ${ROOTFS} "sh /tmp/${pill}"
              done
            fi
            
            if [ '${USER_LIST[@]}' != "" ]; then
              info_p "Running user pills"
              for pill in ${USER_LIST[@]}; do
                  chroot_rootfs ${ROOTFS} "sh /tmp/${pill}"
              done
            fi
            
            if [ '${AUR_LIST[@]}' != "" ]; then
              info_p "Running AUR pills"
              for pill in ${AUR_LIST[@]}; do
                  chroot_rootfs ${ROOTFS} "sh /tmp/${pill}"
              done
            fi
            
            if [ '${EXTRAS_LIST[@]}' != "" ]; then
              info_p "Running extras pills"
              for pill in ${EXTRAS_LIST[@]}; do
                  chroot_rootfs ${ROOTFS} "sh /tmp/${pill}"
              done
            fi
    
    5.  Modified pillbox.sh
    
        Our modified pillbox.sh should now look like this.
        
            #!/bin/bash
            
            ## Scripts dir
            SCRIPTS=$(dirname $0)
            
            ## Required libs/scripts
            . ./${SCRIPTS}/global_vars
            . ./${SCRIPTS}/global_functions.sh
            . ./${SCRIPTS}/info_messages.sh
            . ./${SCRIPTS}/checks.sh
            
            ## Pill dirs
            SYS="${SCRIPTS}/pills/system_pills"
            USER="${SCRIPTS}/pills/user_pills"
            AUR="${SCRIPTS}/pills/aur_pills"
            EXTRAS="${SCRIPTS}/pills/extras_pills"
            
            ## Pillbox Addon Manager ##
            pillbox() {
                # Create pill lists
                info_p "Getting index of pills"
                readarray -t SYS_LIST <<<$(ls -1 ${SYS})
                readarray -t USER_LIST <<<$(ls -1 ${USER})
                readarray -t AUR_LIST <<<$(ls -1 ${AUR}) 
                readarray -t EXTRAS_LIST <<<$(ls -1 ${EXTRAS})
            
                # Copy pills from lists to /tmp in rootfs
                if [ '${SYS_LIST[@]}' != "" ]; then
                  info_p "Copying system pills"
                  for pill in ${SYS_LIST[@]}; do
              	  cp ${SYS}/${pill} ${ROOTFS}/tmp/
                  done
                fi
            
                if [ '${USER_LIST[@]}' != "" ]; then
                  info_p "Copying user pills"
                  for pill in ${USER_LIST[@]}; do
              	  cp ${USER}/${pill} ${ROOTFS}/tmp/
                  done
                fi
            
                if [ '${AUR_LIST[@]}' != "" ]; then
                  info_p "Copying AUR pills"
                  for pill in ${AUR_LIST[@]}; do
              	  cp ${AUR}/${pill} ${ROOTFS}/tmp/
                  done
                fi
            
                if [ '${EXTRAS_LIST[@]}' != "" ]; then
                  info_p "Copying extras pills"
                  for pill in ${EXTRAS_LIST[@]}; do
                      cp ${EXTRAS}/${pill} ${ROOTFS}/tmp/
                  done
                fi
            
                # Start running pill scripts
                if [ '${SYS_LIST[@]}' != "" ]; then
                  info_p "Running system pills"
                  for pill in ${SYS_LIST[@]}; do
              	  chroot_rootfs ${ROOTFS} "sh /tmp/${pill}"
                  done
                fi
            
                if [ '${USER_LIST[@]}' != "" ]; then
                  info_p "Running user pills"
                  for pill in ${USER_LIST[@]}; do
              	  chroot_rootfs ${ROOTFS} "sh /tmp/${pill}"
                  done
                fi
            
                if [ '${AUR_LIST[@]}' != "" ]; then
                  info_p "Running AUR pills"
                  for pill in ${AUR_LIST[@]}; do
              	  chroot_rootfs ${ROOTFS} "sh /tmp/${pill}"
                  done
                fi
            
                if [ '${EXTRAS_LIST[@]}' != "" ]; then
                  info_p "Running extras pills"
                  for pill in ${EXTRAS_LIST[@]}; do
                      chroot_rootfs ${ROOTFS} "sh /tmp/${pill}"
                  done
                fi
            }
            
            pillbox
        
        Let's test it shall we?


<a id="org94d9900"></a>

### Creating Our Patch

Before we get ahead of ourselves we need to make the patch file. To do so we will be using a tool called "diff". Make sure to cd into
scripts/ if you have not done so already.

To create our patch file we will use diff like so.

    diff -u pillbox.sh new_pillbox.sh > cool_pillbox_patch

What diff does here is it gets the differences between the two files and logs it into our patch file. (Hence the name "diff") You can
open the patch file up in a text editor to see what is output or just run diff without the redirect to our patch file.


<a id="org9592c62"></a>

### Applying Our Patch

To apply our patch we're going to use a tool called "patch", but first let's make sure to make a backup of pillbox.sh like so.

    cp pillbox.sh pillbox.sh.bak

Now that we have done that we can apply our patch.

    patch pillbox.sh < cool_pillbox_patch


<a id="orge7706d6"></a>

### Testing Our Patch

Now that we have applied the patch let's take our "hello world" pill example from "Writing Pills" in the Pillbox documentation and put
it in our new scripts/pills/extras\_pills directory.

    mv scripts/pills/system_pills/PILL_hello_world.sh scripts/pills/extras_pills/

Now that we moved over our "hello world" example we can now test out our new pill type.

    sudo make rootfs

After artix-bootstrap does its thing and pillbox is invoked we should see our new pill type pop up with a info message.

    [PILLBOX] > Getting index of pills
    [PILLBOX] > Copying system pills
    [PILLBOX] > Copying user pills
    [PILLBOX] > Copying AUR pills
    [PILLBOX] > Copying extras pills

Let's now wait for our "hello world" pill to run.

We should now see our "hello world" pill being ran from our new pill type.

    [PILLBOX] > Running extras pills
    resolving dependencies...
    looking for conflicting packages...
    warning: dependency cycle detected:
    warning: rubygems will be installed before its ruby dependency
    warning: dependency cycle detected:
    warning: ruby-bundler will be installed before its ruby dependency
    warning: dependency cycle detected:
    warning: ruby-irb will be installed before its ruby dependency
    warning: dependency cycle detected:
    warning: ruby-reline will be installed before its ruby dependency
    warning: dependency cycle detected:
    warning: ruby-json will be installed before its ruby dependency
    warning: dependency cycle detected:
    warning: ruby-psych will be installed before its ruby dependency
    warning: dependency cycle detected:
    warning: ruby-stringio will be installed before its ruby dependency
    warning: dependency cycle detected:
    warning: ruby-rdoc will be installed before its ruby dependency
    warning: dependency cycle detected:
    warning: ruby-minitest will be installed before its ruby dependency
    warning: dependency cycle detected:
    warning: ruby-power_assert will be installed before its ruby dependency
    warning: dependency cycle detected:
    warning: ruby-rake will be installed before its ruby dependency
    warning: dependency cycle detected:
    warning: ruby-rexml will be installed before its ruby dependency
    warning: dependency cycle detected:
    warning: ruby-test-unit will be installed before its ruby dependency
    
    Packages (31) libyaml-0.2.5-1.1  ruby-3.0.4-2  ruby-bundledgems-3.0.4-2  ruby-bundler-2.3.17-1  ruby-diff-lcs-1.4.4-2  ruby-irb-1.4.1-1
                  ruby-json-2.6.2-1  ruby-manpages-0.6.1-4  ruby-minitest-5.15.0-1  ruby-optimist-3.0.1-2  ruby-paint-2.2.1-2
                  ruby-power_assert-2.0.1-1  ruby-psych-4.0.4-2  ruby-rainbow-3.0.0-6  ruby-rake-13.0.6-1  ruby-rdoc-6.4.0-2
                  ruby-reline-0.3.1-1  ruby-rexml-3.2.5-1  ruby-rspec-3.10.0-1  ruby-rspec-core-3.10.1-1  ruby-rspec-expectations-3.10.1-1
                  ruby-rspec-mocks-3.10.2-1  ruby-rspec-support-3.10.2-1  ruby-stdlib-3.0.4-2  ruby-stringio-3.0.2-1
                  ruby-term-ansicolor-1.7.1-2  ruby-test-unit-3.5.3-1  ruby-tins-1.31.0-1  rubygems-3.3.15-1.2  figlet-2.2.5-4
                  lolcat-100.0.1-1
    
    Total Download Size:    6.45 MiB
    Total Installed Size:  26.60 MiB
    
    :: Proceed with installation? [Y/n]
    :: Retrieving packages...
     ruby-3.0.4-2-x86_64                                    3.3 MiB  2.82 MiB/s 00:01 [###############################################] 100%
     ruby-rdoc-6.4.0-2-any                                472.1 KiB  2.88 MiB/s 00:00 [###############################################] 100%
     ruby-bundler-2.3.17-1-any                            380.7 KiB  2.48 MiB/s 00:00 [###############################################] 100%
     ruby-rspec-core-3.10.1-1-any                         323.4 KiB  2.21 MiB/s 00:00 [###############################################] 100%
     rubygems-3.3.15-1.2-any                              232.1 KiB  1694 KiB/s 00:00 [###############################################] 100%
     ruby-test-unit-3.5.3-1-any                           203.9 KiB  1488 KiB/s 00:00 [###############################################] 100%
     ruby-rspec-expectations-3.10.1-1-any                 187.7 KiB  1412 KiB/s 00:00 [###############################################] 100%
     ruby-rspec-mocks-3.10.2-1-any                        181.5 KiB  1365 KiB/s 00:00 [###############################################] 100%
     lolcat-100.0.1-1-any                                 120.2 KiB   925 KiB/s 00:00 [###############################################] 100%
     figlet-2.2.5-4-x86_64                                105.4 KiB   850 KiB/s 00:00 [###############################################] 100%
     ruby-tins-1.31.0-1-any                                98.0 KiB   777 KiB/s 00:00 [###############################################] 100%
     ruby-json-2.6.2-1-x86_64                              93.3 KiB   778 KiB/s 00:00 [###############################################] 100%
     ruby-psych-4.0.4-2-x86_64                             91.8 KiB   723 KiB/s 00:00 [###############################################] 100%
     ruby-rexml-3.2.5-1-any                                82.0 KiB   646 KiB/s 00:00 [###############################################] 100%
     ruby-rspec-support-3.10.2-1-any                       79.8 KiB   633 KiB/s 00:00 [###############################################] 100%
     ruby-diff-lcs-1.4.4-2-any                             75.3 KiB   643 KiB/s 00:00 [###############################################] 100%
     ruby-rake-13.0.6-1-any                                75.1 KiB   626 KiB/s 00:00 [###############################################] 100%
     ruby-minitest-5.15.0-1-any                            71.2 KiB   560 KiB/s 00:00 [###############################################] 100%
     ruby-irb-1.4.1-1-any                                  65.8 KiB   535 KiB/s 00:00 [###############################################] 100%
     libyaml-0.2.5-1.1-x86_64                              58.1 KiB   472 KiB/s 00:00 [###############################################] 100%
     ruby-reline-0.3.1-1-any                               48.8 KiB   406 KiB/s 00:00 [###############################################] 100%
     ruby-term-ansicolor-1.7.1-2-any                       41.7 KiB   337 KiB/s 00:00 [###############################################] 100%
     ruby-stringio-3.0.2-1-any                             34.7 KiB   300 KiB/s 00:00 [###############################################] 100%
     ruby-optimist-3.0.1-2-any                             34.7 KiB   280 KiB/s 00:00 [###############################################] 100%
     ruby-rainbow-3.0.0-6-any                              32.5 KiB   280 KiB/s 00:00 [###############################################] 100%
     ruby-power_assert-2.0.1-1-any                         30.2 KiB   252 KiB/s 00:00 [###############################################] 100%
     ruby-paint-2.2.1-2-any                                25.1 KiB   220 KiB/s 00:00 [###############################################] 100%
     ruby-manpages-0.6.1-4-any                             19.7 KiB   170 KiB/s 00:00 [###############################################] 100%
     ruby-rspec-3.10.0-1-any                                7.7 KiB  67.8 KiB/s 00:00 [###############################################] 100%
     ruby-stdlib-3.0.4-2-x86_64                             2.7 KiB  23.8 KiB/s 00:00 [###############################################] 100%
     ruby-bundledgems-3.0.4-2-x86_64                        2.7 KiB  22.8 KiB/s 00:00 [###############################################] 100%
     Total (31/31)                                          6.5 MiB  1339 KiB/s 00:05 [###############################################] 100%
    (31/31) checking keys in keyring                                                  [###############################################] 100%
    (31/31) checking package integrity                                                [###############################################] 100%
    (31/31) loading package files                                                     [###############################################] 100%
    (31/31) checking for file conflicts                                               [###############################################] 100%
    :: Processing package changes...
    ( 1/31) installing libyaml                                                        [###############################################] 100%
    ( 2/31) installing rubygems                                                       [###############################################] 100%
    ( 3/31) installing ruby-bundler                                                   [###############################################] 100%
    ( 4/31) installing ruby-reline                                                    [###############################################] 100%
    ( 5/31) installing ruby-irb                                                       [###############################################] 100%
    ( 6/31) installing ruby-json                                                      [###############################################] 100%
    ( 7/31) installing ruby-stringio                                                  [###############################################] 100%
    ( 8/31) installing ruby-psych                                                     [###############################################] 100%
    ( 9/31) installing ruby-rdoc                                                      [###############################################] 100%
    (10/31) installing ruby-stdlib                                                    [###############################################] 100%
    (11/31) installing ruby-minitest                                                  [###############################################] 100%
    (12/31) installing ruby-power_assert                                              [###############################################] 100%
    (13/31) installing ruby-rake                                                      [###############################################] 100%
    (14/31) installing ruby-rexml                                                     [###############################################] 100%
    (15/31) installing ruby-test-unit                                                 [###############################################] 100%
    (16/31) installing ruby-bundledgems                                               [###############################################] 100%
    (17/31) installing ruby                                                           [###############################################] 100%
    Optional dependencies for ruby
        ruby-docs: Ruby documentation
        tk: for Ruby/TK
    (18/31) installing ruby-rspec-support                                             [###############################################] 100%
    (19/31) installing ruby-rspec-core                                                [###############################################] 100%
    (20/31) installing ruby-diff-lcs                                                  [###############################################] 100%
    (21/31) installing ruby-rspec-expectations                                        [###############################################] 100%
    (22/31) installing ruby-rspec-mocks                                               [###############################################] 100%
    (23/31) installing ruby-rspec                                                     [###############################################] 100%
    (24/31) installing ruby-rainbow                                                   [###############################################] 100%
    (25/31) installing ruby-tins                                                      [###############################################] 100%
    (26/31) installing ruby-term-ansicolor                                            [###############################################] 100%
    (27/31) installing ruby-paint                                                     [###############################################] 100%
    (28/31) installing ruby-optimist                                                  [###############################################] 100%
    (29/31) installing ruby-manpages                                                  [###############################################] 100%
    (30/31) installing lolcat                                                         [###############################################] 100%
    (31/31) installing figlet                                                         [###############################################] 100%
     _   _      _ _        __        __         _     _ _
    | | | | ___| | | ___   \ \      / /__  _ __| | __| | |
    | |_| |/ _ \ | |/ _ \   \ \ /\ / / _ \| '__| |/ _` | |
    |  _  |  __/ | | (_) |   \ V  V / (_) | |  | | (_| |_|
    |_| |_|\___|_|_|\___/     \_/\_/ \___/|_|  |_|\__,_(_)
    
    [INFO] > Cleaning up build/addycmos_rootfs


<a id="org999e8ef"></a>

## Modifying Tweaker DBG For Our Pill Type

In this section we will be modifying Tweaker DBG to include our new pill type. Running "sudo make rootfs" to test our new pill type
can become repetitive, boring, and a waste of time. This is where Tweaker DBG comes in. Tweaker DBG will allow us to just run single
pill in our pill type instead of having to wait for it to be executed by Pillbox in the RootFS generation script.


<a id="org417604b"></a>

### Making A Copy of tweaker\_dbg.sh

Similar to before we want to make a copy of tweaker\_dbg.sh, so we can make a patch file later.

    cp scripts/tweaker_dbg.sh scripts/new_tweaker_dbg.sh

After we have done that we can start modifying our copy.


<a id="orgc8ad0f0"></a>

### Editing Our New Copy

We can now start editing our new copy. Same as before we will be using whatever text editor we want. I will be displaying the file and
the modified file same as before.

1.  Unmodified File

    This is our unmodified version of Tweaker DBG.
    
        #!/bin/bash
        
        ## Scripts dir
        SCRIPTS=$(dirname $0)
        
        ## Required libs/scripts
        . ./${SCRIPTS}/global_vars
        . ./${SCRIPTS}/global_functions.sh
        . ./${SCRIPTS}/info_messages.sh
        . ./${SCRIPTS}/checks.sh
        . ./${SCRIPTS}/generate_rootfs.sh
        . ./${SCRIPTS}/create_addycmos_img.sh
        . ./${SCRIPTS}/vmtest.sh
        
        ## Tool for manual debugging within chroot
        dbg_chroot() {
            # Privs check
            checks privs
        
            # Rootfs check
            info_t "Checking if RootFS exists"
            checks rootfs exists
        
            # Pseudo fs already mounted check
            checks pseudo_fs_mount ${ROOTFS}
        
            # Mount pseudo filesystems
            info_t "Mounting pseudo filesystems"
            mount_pseudo ${ROOTFS}
        
            # Chroot into RootFS
            info_t "Entering chroot"
            chroot ${ROOTFS} /bin/bash
        }
        
        ## Tool for running and testing pills
        dbg_pillbox() {
            # Privs check
            checks privs
        
            # Rootfs check
            info_t "Checking if RootFS exists"
            checks rootfs exists
        
            # Just run all the pills using pillbox like normal
            run_all_pills() {
              sh ${SCRIPTS}/pillbox.sh
            }
        
            # Run a single pill using tweaker
            run_single_pill() {
              # Pill dirs
              SYS="${SCRIPTS}/pills/system_pills"
              USER="${SCRIPTS}/pills/user_pills"
              AUR="${SCRIPTS}/pills/aur_pills"
        
              # Create pill lists
              readarray -t SYS_LIST <<<$(ls -1 ${SYS})
              readarray -t USER_LIST <<<$(ls -1 ${USER})
              readarray -t AUR_LIST <<<$(ls -1 ${AUR})
        
              # Pill types
              TYPES=("System" "User" "AUR");
        
              # Select type of pill to be ran
              info_t "Select pill type to be ran"
              select pill_type in ${TYPES[@]}; do
          	  case ${pill_type} in
          	      # Select system pill to be ran
          	      System)
          		  info_t "System selected"
          		  info_t "Select pill to be ran"
          		  select pill in ${SYS_LIST[@]}; do
          		      case ${pill} in
          			  ${pill})
          			      info_t "${pill} selected"
        
          			      # Copy pill over to /tmp in RootFS
          			      info_t "Copying ${pill} over to ${ROOTFS}/tmp"
          			      cp ${SYS}/${pill} ${ROOTFS}/tmp/
        
          			      # Run pill script in RootFS chroot
          			      info_t "Running ${pill} inside chroot at ${ROOTFS}"
          			      chroot_rootfs ${ROOTFS} "sh -x /tmp/${pill};"
        
          			      break
          			  ;;
          		      esac
          		  done
          		  break
          	      ;;
        
          	      # Select user pill to be ran
          	      User)
          		  info_t "User selected"
          		  info_t "Select pill to be ran"
          		  select pill in ${USER_LIST[@]}; do
          		      case ${pill} in
          			  ${pill})
          			      info_t "${pill} selected"
        
          			      # Copy pill over to /tmp in RootFS
          			      info_t "Copying ${pill} over to ${ROOTFS}/tmp"
          			      cp ${USER}/${pill} ${ROOTFS}/tmp/
        
          			      # Run pill script in RootFS chroot
          			      info_t "Running ${pill} inside chroot at ${ROOTFS}"
          			      chroot_rootfs ${ROOTFS} "sh -x /tmp/${pill};"
        
          			      break
          			  ;;
          		      esac
          		  done
          		  break
          	      ;;
        
          	      # Select AUR pill to be ran
          	      AUR)
          		  info_t "AUR selected"
          		  info_t "Select pill to be ran"
          		  select pill in ${AUR_LIST[@]}; do
          		      case ${pill} in
          			  ${pill})
          			      info_t "${pill} selected"
        
          			      # Copy pill over to /tmp in RootFS
          			      info_t "Copying ${pill} over to ${ROOTFS}/tmp"
          			      cp ${AUR}/${pill} ${ROOTFS}/tmp/
        
          			      # Run pill script in RootFS chroot
          			      info_t "Running ${pill} inside chroot at ${ROOTFS}"
          			      chroot_rootfs ${ROOTFS} "sh -x /tmp/${pill};"
        
          			      break
          			  ;;
          		      esac
          		  done
          		  break
          	      ;;
          	  esac
              done
            }
        
            case $1 in
              --run-all-pills)	run_all_pills;;
              --run-pill)		run_single_pill;;
              *)			echo "Please provide --run-all-pills or --run-pill";;
            esac
        }
        
        ## Tool to run RootFS generation with debugging
        dbg_rootfs() {
            # Privs check
            checks privs
        
            # Run RootFS generation with debugging
            info_t "Generating RootFS with debugging"
            set -x
            generate_rootfs
            set +x
        }
        
        ## Tool to run RAW image creation with debugging
        dbg_create_img() {
            # Privs check
            checks privs
        
            # Run RAW image creation with debugginga
            info_t "Creating RAW image with debugging"
            set -x
            create_img
            set +x
        }
        
        ## Arguments
        ARGS=(
            "  --dbg-chroot|tool for manual debugging within chroot  "
            "  --dbg-pillbox (--run-all-pills, --run-pill)|tool for running and testing pills  "
            "  --dbg-rootfs|tool to run RootFS generation with debugging  " 
            "  --dbg-create-img|tool to run RAW image creation with debugging  "
            "  --help, -h|display help  "
        )
        
        ## Help output
        help_output() {
              printf '%s\n\n' "Usage: tweaker_dbg.sh [ARGUMENT] [SUB-ARGUMENT]"
              printf '%s\n' "${ARGS[@]}" | column -t -s'|'
              printf '\n%s\n' "All sub-arguments are mandatory."
              printf '\n%s\n' "Report any bugs to WheezyBackports#7260 on discord." 
        }
        
        case $1 in
            --dbg-chroot)	dbg_chroot;;
            --dbg-pillbox)	dbg_pillbox $2;;
            --dbg-rootfs)	dbg_rootfs;;
            --dbg-create-img)	dbg_create_img;;
            --help | -h)	help_output;;
            *)			help_output;;
        esac

2.  Modded Tweaker DBG

    Now on to our modded version. To add our new pill type to Tweaker DBG we're going to need to add 4 things.
    
    We're going to need to add:
    
    -   A EXTRAS variable that points to our extras\_pills/ directory in scripts/pills
    -   A readarray statement which will read the names of pills into an array called EXTRAS\_LIST
    -   To add our pill type to the TYPES array
    -   Selections for our pill type
    
    1.  Extras Variable
    
        Our EXTRAS variable will look like this alongside the other variables.
        
            ## Pill dirs
            SYS="${SCRIPTS}/pills/system_pills"
            USER="${SCRIPTS}/pills/user_pills"
            AUR="${SCRIPTS}/pills/aur_pills"
            EXTRAS="${SCRIPTS}/pills/extras_pills"
    
    2.  Readarray Statement
    
        Our readarray statement will look like this alongside the other readarray statements.
        
            # Create pill lists
            info_p "Getting index of pills"
            readarray -t SYS_LIST <<<$(ls -1 ${SYS})
            readarray -t USER_LIST <<<$(ls -1 ${USER})
            readarray -t AUR_LIST <<<$(ls -1 ${AUR})
            readarray -t EXTRAS_LIST <<<$(ls -1 ${EXTRAS})
    
    3.  Types Array
    
        Our TYPES array will look like this alongside the other elements.
        
            # Pill types
            TYPES=("System" "User" "AUR" "Extras");
    
    4.  Selections
    
        Our selections will look like this alongside the other selections
        
            # Select type of pill to be ran
            info_t "Select pill type to be ran"
            select pill_type in ${TYPES[@]}; do
                case ${pill_type} in
                  # Select system pill to be ran
                  System)
              	  info_t "System selected"
              	  info_t "Select pill to be ran"
              	  select pill in ${SYS_LIST[@]}; do
              	      case ${pill} in
              		  ${pill})
              		      info_t "${pill} selected"
            
              		      # Copy pill over to /tmp in RootFS
              		      info_t "Copying ${pill} over to ${ROOTFS}/tmp"
              		      cp ${SYS}/${pill} ${ROOTFS}/tmp/
            
              		      # Run pill script in RootFS chroot
              		      info_t "Running ${pill} inside chroot at ${ROOTFS}"
              		      chroot_rootfs ${ROOTFS} "sh -x /tmp/${pill};"
            
              		      break
              		  ;;
              	      esac
              	  done
              	  break
                  ;;
            
                  # Select user pill to be ran
                  User)
              	  info_t "User selected"
              	  info_t "Select pill to be ran"
              	  select pill in ${USER_LIST[@]}; do
              	      case ${pill} in
              		  ${pill})
              		      info_t "${pill} selected"
            
              		      # Copy pill over to /tmp in RootFS
              		      info_t "Copying ${pill} over to ${ROOTFS}/tmp"
              		      cp ${USER}/${pill} ${ROOTFS}/tmp/
            
              		      # Run pill script in RootFS chroot
              		      info_t "Running ${pill} inside chroot at ${ROOTFS}"
              		      chroot_rootfs ${ROOTFS} "sh -x /tmp/${pill};"
            
              		      break
              		  ;;
              	      esac
              	  done
              	  break
                  ;;
            
                  # Select AUR pill to be ran
                  AUR)
              	  info_t "AUR selected"
              	  info_t "Select pill to be ran"
              	  select pill in ${AUR_LIST[@]}; do
              	      case ${pill} in
              		  ${pill})
              		      info_t "${pill} selected"
            
              		      # Copy pill over to /tmp in RootFS
              		      info_t "Copying ${pill} over to ${ROOTFS}/tmp"
              		      cp ${AUR}/${pill} ${ROOTFS}/tmp/
            
              		      # Run pill script in RootFS chroot
              		      info_t "Running ${pill} inside chroot at ${ROOTFS}"
              		      chroot_rootfs ${ROOTFS} "sh -x /tmp/${pill};"
            
              		      break
              		  ;;
              	      esac
              	  done
              	  break
                  ;;
            
                  # Select Extras pill to be ran
                  Extras)
              	  info_t "Extras selected"
              	  info_t "Select pill to be ran"
              	  select pill in ${EXTRAS_LIST[@]}; do
              	      case ${pill} in
              		  ${pill})
              		      info_t "${pill} selected"
            
              		      # Copy pill over to /tmp in RootFS
              		      info_t "Copying ${pill} over to ${ROOTFS}/tmp"
              		      cp ${EXTRAS}/${pill} ${ROOTFS}/tmp/
            
              		      # Run pill script in RootFS chroot
              		      info_t "Running ${pill} inside chroot at ${ROOTFS}"
              		      chroot_rootfs ${ROOTFS} "sh -x /tmp/${pill};"
            
              		      break
              		  ;;
              	      esac
              	  done
              	  break
                  ;;
                esac
            done
    
    5.  Modified tweaker\_dbg.sh
    
        Our modified tweaker\_dbg.sh should now look like this.
        
            #!/bin/bash
            
            ## Scripts dir
            SCRIPTS=$(dirname $0)
            
            ## Required libs/scripts
            . ./${SCRIPTS}/global_vars
            . ./${SCRIPTS}/global_functions.sh
            . ./${SCRIPTS}/info_messages.sh
            . ./${SCRIPTS}/checks.sh
            . ./${SCRIPTS}/generate_rootfs.sh
            . ./${SCRIPTS}/create_addycmos_img.sh
            . ./${SCRIPTS}/vmtest.sh
            
            ## Tool for manual debugging within chroot
            dbg_chroot() {
                # Privs check
                checks privs
            
                # Rootfs check
                info_t "Checking if RootFS exists"
                checks rootfs exists
            
                # Pseudo fs already mounted check
                checks pseudo_fs_mount ${ROOTFS}
            
                # Mount pseudo filesystems
                info_t "Mounting pseudo filesystems"
                mount_pseudo ${ROOTFS}
            
                # Chroot into RootFS
                info_t "Entering chroot"
                chroot ${ROOTFS} /bin/bash
            }
            
            ## Tool for running and testing pills
            dbg_pillbox() {
                # Privs check
                checks privs
            
                # Rootfs check
                info_t "Checking if RootFS exists"
                checks rootfs exists
            
                # Just run all the pills using pillbox like normal
                run_all_pills() {
                  sh ${SCRIPTS}/pillbox.sh
                }
            
                # Run a single pill using tweaker
                run_single_pill() {
                  # Pill dirs
                  SYS="${SCRIPTS}/pills/system_pills"
                  USER="${SCRIPTS}/pills/user_pills"
                  AUR="${SCRIPTS}/pills/aur_pills"
                  EXTRAS="${SCRIPTS}/pills/extras_pills"
            
                  # Create pill lists
                  readarray -t SYS_LIST <<<$(ls -1 ${SYS})
                  readarray -t USER_LIST <<<$(ls -1 ${USER})
                  readarray -t AUR_LIST <<<$(ls -1 ${AUR})
                  readarray -t EXTRAS_LIST <<<$(ls -1 ${EXTRAS})
            
                  # Pill types
                  TYPES=("System" "User" "AUR" "Extras");
            
                  # Select type of pill to be ran
                  info_t "Select pill type to be ran"
                  select pill_type in ${TYPES[@]}; do
              	  case ${pill_type} in
              	      # Select system pill to be ran
              	      System)
              		  info_t "System selected"
              		  info_t "Select pill to be ran"
              		  select pill in ${SYS_LIST[@]}; do
              		      case ${pill} in
              			  ${pill})
              			      info_t "${pill} selected"
            
              			      # Copy pill over to /tmp in RootFS
              			      info_t "Copying ${pill} over to ${ROOTFS}/tmp"
              			      cp ${SYS}/${pill} ${ROOTFS}/tmp/
            
              			      # Run pill script in RootFS chroot
              			      info_t "Running ${pill} inside chroot at ${ROOTFS}"
              			      chroot_rootfs ${ROOTFS} "sh -x /tmp/${pill};"
            
              			      break
              			  ;;
              		      esac
              		  done
              		  break
              	      ;;
            
              	      # Select user pill to be ran
              	      User)
              		  info_t "User selected"
              		  info_t "Select pill to be ran"
              		  select pill in ${USER_LIST[@]}; do
              		      case ${pill} in
              			  ${pill})
              			      info_t "${pill} selected"
            
              			      # Copy pill over to /tmp in RootFS
              			      info_t "Copying ${pill} over to ${ROOTFS}/tmp"
              			      cp ${USER}/${pill} ${ROOTFS}/tmp/
            
              			      # Run pill script in RootFS chroot
              			      info_t "Running ${pill} inside chroot at ${ROOTFS}"
              			      chroot_rootfs ${ROOTFS} "sh -x /tmp/${pill};"
            
              			      break
              			  ;;
              		      esac
              		  done
              		  break
              	      ;;
            
              	      # Select AUR pill to be ran
              	      AUR)
              		  info_t "AUR selected"
              		  info_t "Select pill to be ran"
              		  select pill in ${AUR_LIST[@]}; do
              		      case ${pill} in
              			  ${pill})
              			      info_t "${pill} selected"
            
              			      # Copy pill over to /tmp in RootFS
              			      info_t "Copying ${pill} over to ${ROOTFS}/tmp"
              			      cp ${AUR}/${pill} ${ROOTFS}/tmp/
            
              			      # Run pill script in RootFS chroot
              			      info_t "Running ${pill} inside chroot at ${ROOTFS}"
              			      chroot_rootfs ${ROOTFS} "sh -x /tmp/${pill};"
            
              			      break
              			  ;;
              		      esac
              		  done
              		  break
              	      ;;
            
              	      # Select Extras pill to be ran
              	      Extras)
              		  info_t "Extras selected"
              		  info_t "Select pill to be ran"
              		  select pill in ${EXTRAS_LIST[@]}; do
              		      case ${pill} in
              			  ${pill})
              			      info_t "${pill} selected"
            
              			      # Copy pill over to /tmp in RootFS
              			      info_t "Copying ${pill} over to ${ROOTFS}/tmp"
              			      cp ${EXTRAS}/${pill} ${ROOTFS}/tmp/
            
              			      # Run pill script in RootFS chroot
              			      info_t "Running ${pill} inside chroot at ${ROOTFS}"
              			      chroot_rootfs ${ROOTFS} "sh -x /tmp/${pill};"
            
              			      break
              			  ;;
              		      esac
              		  done
              		  break
              	      ;;
              	  esac
                  done
                }
            
                case $1 in
                  --run-all-pills)	run_all_pills;;
                  --run-pill)		run_single_pill;;
                  *)			echo "Please provide --run-all-pills or --run-pill";;
                esac
            }
            
            ## Tool to run RootFS generation with debugging
            dbg_rootfs() {
                # Privs check
                checks privs
            
                # Run RootFS generation with debugging
                info_t "Generating RootFS with debugging"
                set -x
                generate_rootfs
                set +x
            }
            
            ## Tool to run RAW image creation with debugging
            dbg_create_img() {
                # Privs check
                checks privs
            
                # Run RAW image creation with debugginga
                info_t "Creating RAW image with debugging"
                set -x
                create_img
                set +x
            }
            
            ## Arguments
            ARGS=(
                "  --dbg-chroot|tool for manual debugging within chroot  "
                "  --dbg-pillbox (--run-all-pills, --run-pill)|tool for running and testing pills  "
                "  --dbg-rootfs|tool to run RootFS generation with debugging  " 
                "  --dbg-create-img|tool to run RAW image creation with debugging  "
                "  --help, -h|display help  "
            )
            
            ## Help output
            help_output() {
                  printf '%s\n\n' "Usage: tweaker_dbg.sh [ARGUMENT] [SUB-ARGUMENT]"
                  printf '%s\n' "${ARGS[@]}" | column -t -s'|'
                  printf '\n%s\n' "All sub-arguments are mandatory."
                  printf '\n%s\n' "Report any bugs to WheezyBackports#7260 on discord." 
            }
            
            case $1 in
                --dbg-chroot)	dbg_chroot;;
                --dbg-pillbox)	dbg_pillbox $2;;
                --dbg-rootfs)	dbg_rootfs;;
                --dbg-create-img)	dbg_create_img;;
                --help | -h)	help_output;;
                *)			help_output;;
            esac


<a id="org234d4d3"></a>

### Creating Our Patch

Like before with pillbox.sh we can now create our patch file.

    diff -u tweaker_dbg.sh new_tweaker_dbg.sh > cool_tweaker_patch


<a id="org9683630"></a>

### Applying Our Patch

Same as before we make a backup and apply our patch.

    cp tweaker_dbg.sh tweaker_dbg.sh.bak

    patch tweaker_dbg.sh < cool_tweaker_patch


<a id="orga3ac13f"></a>

### Testing Our Patch

We can now test running our pill with the new patched Tweaker DBG.

    sudo sh scripts/tweaker_dbg.sh --dbg-pillbox --run-pill

We should get a selection prompt like this.

    [Tweaker DBG] > Select pill type to be ran
    1) System
    2) User
    3) AUR
    4) Extras
    #?

We select our pill type by entering the corresponding number.

We should get another selection prompt like this.

    [Tweaker DBG] > Select pill to be ran
    1) PILL_hello_world.sh
    #?

We select the pill to be ran by entering the corresponding number.

We should see an output like so.

    [Tweaker DBG] > PILL_hello_world.sh selected
    [Tweaker DBG] > Copying PILL_hello_world.sh over to build/addycmos_rootfs/tmp
    [Tweaker DBG] > Running PILL_hello_world.sh inside chroot at build/addycmos_rootfs
    + PKGS=("lolcat" "figlet")
    + install_pkgs
    + pacman -S --noconfirm lolcat figlet
    warning: lolcat-100.0.1-1 is up to date -- reinstalling
    warning: figlet-2.2.5-4 is up to date -- reinstalling
    resolving dependencies...
    looking for conflicting packages...
    
    Packages (2) figlet-2.2.5-4  lolcat-100.0.1-1
    
    Total Installed Size:  0.76 MiB
    Net Upgrade Size:      0.00 MiB
    
    :: Proceed with installation? [Y/n]
    (2/2) checking keys in keyring                                                    [###############################################] 100%
    (2/2) checking package integrity                                                  [###############################################] 100%
    (2/2) loading package files                                                       [###############################################] 100%
    (2/2) checking for file conflicts                                                 [###############################################] 100%
    :: Processing package changes...
    (1/2) reinstalling lolcat                                                         [###############################################] 100%
    (2/2) reinstalling figlet                                                         [###############################################] 100%
    + cool_ascii
    + figlet Hello 'World!'
    + lolcat
     _   _      _ _        __        __         _     _ _
    | | | | ___| | | ___   \ \      / /__  _ __| | __| | |
    | |_| |/ _ \ | |/ _ \   \ \ /\ / / _ \| '__| |/ _` | |
    |  _  |  __/ | | (_) |   \ V  V / (_) | |  | | (_| |_|
    |_| |_|\___|_|_|\___/     \_/\_/ \___/|_|  |_|\__,_(_)

Congratulations you have now successfully modified Pillbox and Tweaker DBG! The "Extras" pill type will be included with the dev kit.
This documentation was written while developing the new feature as a way to help me demonstrate the modding capabilities.

