all:
	sh scripts/main.sh --generate-rootfs
	sh scripts/main.sh --create-img

rootfs:
	sh scripts/main.sh --generate-rootfs

img: 
	sh scripts/main.sh --create-img

vmtest:
	sh scripts/main.sh --vmtest

